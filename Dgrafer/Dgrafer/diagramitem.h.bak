#ifndef DIAGRAMITEM_H
#define DIAGRAMITEM_H

#include <QGraphicsPixmapItem>
#include <QList>

QT_BEGIN_NAMESPACE
class QPixmap;
class QGraphicsItem;
class QGraphicsScene;
class QTextEdit;
class QGraphicsSceneMouseEvent;
class QMenu;
class QGraphicsSceneContextMenuEvent;
class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;
class QPolygonF;
QT_END_NAMESPACE

class Arrow;

class DiagramItem : public QGraphicsPolygonItem
{
public:
	int returnId() const	//возвращает id пакета
		{return id;}
	bool getIsParent() const	//являетс ли этот пакет родительским
		{return isParent;}
	bool getIsChild() const	//является ли этот пакет дочерним
		{return isChild;}
	bool getIsPlus() const
		{return plus;}
	bool canGetIsParent();
	bool getIsComment() const
		{return isComment;}
	
	QList <DiagramItem *> getListChild()	//возвращает список дочерних пакетов
		{return listChild;}
	QPolygonF getMyPolygon()
		{return myPolygon;}
	int returnCountArrow(DiagramItem * item)	//возвращает количество связей у элемента
		{return item->arrows.count();}
	void delArrow(DiagramItem * item, int n)
	{
		item->arrows.removeAt(n);
	}

	Arrow * returnStartItem(DiagramItem * item, int n)
	{		
		 return item->arrows.at(n);		 
	}
	
	DiagramItem * getItemId(DiagramItem * item__, int id_);

	enum { Type = UserType + 15 };
	enum DiagramType { Step, Comment, Plus, Minus};

	DiagramItem(DiagramType diagramType, QMenu *contextMenu,
		QGraphicsItem *parent = 0, QGraphicsScene *scene = 0);

	void removeArrow(Arrow *arrow);
	void removeArrows();
	DiagramType diagramType() const
		{ return myDiagramType; }
	QPolygonF polygon() const
		{ return myPolygon; }
	void addArrow(Arrow *arrow);
		QPixmap image() const;
	int type() const
		{ return Type;}
	void resizePoligonMax();	//изменение размера полигона при его увеличении
	void resizePoligonMin();	//изменение размера полигона при его уменьшении

	
	void setId(int id_)	//задает идентификатор
		{id=id_;}
	void setIsChild(bool _isChild)	//задает зависимость
		{isChild = _isChild;}
	void setIsParent(bool _isParent)	//задает зависимость
		{isParent = _isParent;}
	void addListChild(DiagramItem * itemChild)	//добавляет в список дочерних пакетов новый пакет
		{listChild << itemChild;}
	void delChildInList(DiagramItem * itemChild)	//добавляет в список дочерних пакетов новый пакет
		{listChild.removeOne(itemChild);}
	void setIsPlus(bool isPlus)
		{plus = isPlus;}
	void setIsComment(bool set)
		{isComment = set;}

protected:
	void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
	QVariant itemChange(GraphicsItemChange change, const QVariant &value);

private:
	int id;	//идентификатор пакета
	bool isComment;
	bool plus;	//открытый\закрытый пакет
	bool isParent;	//пакет является родителем
	bool isChild;	//пакет является дочерним
	QList <DiagramItem *> listChild;	//список идентификаторов дочерних пакетов
	DiagramType myDiagramType;
	QPolygonF myPolygon;
	QMenu *myContextMenu;
	QList<Arrow *> arrows;
};

#endif
