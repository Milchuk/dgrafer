#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include "ui_mainwindow.h"

#include "diagramitem.h"
#include <QSettings>
#include <windows.h>
#include <QMouseEvent>

#include <QtGui>
#include <QLabel>
#include <QFile>
#include <QPushButton>
#include <QPixmap>


#include "diagramscene.h"
#include "diagramtextitem.h"
#include "arrow.h"
class DiagramScene;

QT_BEGIN_NAMESPACE
class QAction;
class QToolBox;
class QSpinBox;
class QComboBox;
class QFontComboBox;
class QButtonGroup;
class QLineEdit;
class QGraphicsTextItem;
class QFont;
class QToolButton;
class QAbstractButton;
class QGraphicsView;
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = 0, Qt::WFlags flags = 0);
	~MainWindow();
	void closeEvent(QCloseEvent * event);//�������� ��������� �������� ���������

	private slots:
		void setTextInsert();//������� ���������� ����������
		void setLineInsert();//������� ���������� �����
		void setItemInsert();//������� ���������� ������
		void deleteItem();//������� �������� ��������
		void itemInserted(DiagramItem *item);
		void textInserted(QGraphicsTextItem *item);
		void itemSelected(QGraphicsItem *item);
		void item_Selected();
		void bringToFront();//������� ����������� ��� ������
		void sendToBack();//������� ����������� ��� �����
		void currentFontChanged(const QFont &font);
		void fontSizeChanged(const QString &size);
		void sceneScaleChanged(const QString &scale);
		void textButtonTriggered();
		void fillButtonTriggered();
		void lineButtonTriggered();
		void textColorChanged();
		void itemColorChanged();
		void lineColorChanged();
		void handleFontChange();
		void backgroundButtonGroupClicked(QAbstractButton *button);
		void buttonGroupClicked(int id);
		void pointerGroupClicked(int id);
		void sl_saveSceneToFile();
		void sl_loadSceneFromFile();
		void loadMasElement(int a);
		void loadElements(QString str, int count);
		void loadone();
		void loadtwo();
		void loadthree();
		void export_picture();
		void clearItem();
		void print();
		void aboutProj();
		void Help();
		void readSettings();
		void writeSettings();


private:
	QString fileName;//�����������
	QString fileNameLoad;//�����������
	QString fileNamePic;// �������������� � ��������
	void createToolBox();
	void createMenus();
	void createActions();
	void createToolbars();
	QWidget *createBackgroundCellWidget(const QString &text,
		const QString &image);
	QWidget *createCellWidget(const QString &text,
		DiagramItem::DiagramType type);
	QMenu *createColorMenu(const char *slot, QColor defaultColor);
	QIcon createColorToolButtonIcon(const QString &image, QColor color);
	QIcon createColorIcon(QColor color);
	
	Ui::MainWindowClass ui;
	DiagramScene *scene;
	QGraphicsView *view;

	//QAction *exitAction;
	QAction *addAction;
	QAction *deleteAction;
	QAction *clearAll;

	QAction *toFrontAction;
	QAction *sendBackAction;
	QAction *aboutAction;
	QAction *aboutActionPr;//����� � ���������
	QAction *help;//����� �����
	QAction *packetAction;
	QAction *noteAction;

	QMenu *fileMenu;
	QMenu *itemMenu;
	QMenu *aboutMenu;
	QMenu *helpMenu;

	QToolBar *textToolBar;
	QToolBar *editToolBar;
	QToolBar *colorToolBar;
	QToolBar *pointerToolbar;

	QComboBox *sceneScaleCombo;
	QComboBox *itemColorCombo;
	QComboBox *textColorCombo;
	QComboBox *fontSizeCombo;
	QFontComboBox *fontCombo;

	QToolBox *toolBox;
	QButtonGroup *buttonGroup;
	QButtonGroup *pointerTypeGroup;
	QButtonGroup *backgroundButtonGroup;
	QToolButton *fontColorToolButton;
	QToolButton *fillColorToolButton;
	QToolButton *lineColorToolButton;
	QAction *boldAction;
	QAction *underlineAction;
	QAction *italicAction;
	QAction *textAction;
	QAction *fillAction;
	QAction *lineAction;

	
	QSettings settings;
};

#endif // MAINWINDOW_H
