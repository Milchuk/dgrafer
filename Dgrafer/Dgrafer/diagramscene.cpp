#include <QtGui>

#include "diagramscene.h"
#include "arrow.h"

#include <typeinfo.h>
#include <string>
#include <iostream>
#include <stdio.h>

using namespace std;

//����� ���������� myItemMenu ��� ��������� ������������ ����, ����� ��� ������ DiagramItems. 
//�� ������������� ����� �� ��������� DiagramScene::MoveItem ��� ��� �� ������������� ��������� QGraphicsScene ��-���������.

DiagramScene::DiagramScene(QMenu *itemMenu, QObject *parent)
	: QGraphicsScene(parent)
{
	maxId = 0;
	countPacket = 0;
	zValuePacket = 10;
	myItemMenu = itemMenu;
	myMode = MoveItem;
	myItemType = DiagramItem::Step;
	line = 0;
	textItem = 0;
	myItemColor = Qt::white;
	myTextColor = Qt::black;
	myLineColor = Qt::black;
}

void DiagramScene::setMode(Mode mode)
{
	myMode = mode;
}

void DiagramScene::setItemType(DiagramItem::DiagramType type)
{
	myItemType = type;
}

//��������� ������ ������ ���������� �������� "�������� ������"
void DiagramScene::editorLostFocusName(DiagramTextItem *item)
{
	QTextCursor cursor = item->textCursor();
	cursor.clearSelection();
	item->setTextCursor(cursor);

	QString str;

	//��� ������ �������� �������� ������ ��������� ������� "����� �����"
	if (item->toPlainText().isEmpty()) {
		str.setNum(countPacket);
		item->setHtml("����� ����� (" + str +")");
	}

	//�������� ���������� ������� ������
	str = item->toPlainText();
	if(str.size() > 15)
	{
		str.remove(15,10000);
		item->setHtml(str);
	}
}

//��������� ������ ������ ���������� �������� "�������� ������"
void DiagramScene::editorLostFocusDescription(DiagramTextItem *item)
{
	QTextCursor cursor = item->textCursor();
	cursor.clearSelection();
	item->setTextCursor(cursor);

	QString str;

	//��� ������ �������� �������� ������ ��������� ������� "�������� ������"
	if (item->toPlainText().isEmpty()) {
		item->setHtml("�������� ������");
	}

	//�������� ���������� ������� ������
	str = item->toPlainText();
	if(str.size() > 25)
	{
		str.remove(25,10000);
		item->setHtml(str);
	}
}

//��������� ������ ������ ���������� �������� "�����������"
void DiagramScene::editorLostClass(DiagramTextItem *item)
{
	QTextCursor cursor = item->textCursor();
	cursor.clearSelection();
	item->setTextCursor(cursor);

	QString str;

	//��� ������ �������� �������� ������ ��������� ������� "����� �����"
	if (item->toPlainText().isEmpty()) 
	{
		item->setHtml("�����������");
	}
	//�������� ���������� ������� ������
	str = item->toPlainText();
	if(str.size() > 65)
	{
		str.remove(65,10000);
		item->setHtml(str);
	}
}

void DiagramScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	if (mouseEvent->button() != Qt::LeftButton)
		return;

	bool isInsertInPacket;	//���� ������������, ��� ���������� ������� ������ ������ ��������� � ��� ��������� �������
	if(itemAt(mouseEvent->scenePos()) == 0)
		isInsertInPacket = false;
	else
		isInsertInPacket = true;

	switch (myMode) {
		//������ ����� DiagramItem � ������������ �� ����� � �����, ��� ��� ���� �����. 
		//����� ��� ��������� ������������ ������� ����� ��� ���������� �����.
	case InsertItem:

		if(myItemType == DiagramItem::DiagramType(0))
		{
			if(isInsertInPacket)
			{	
				insertPacket(mouseEvent, isInsertInPacket);	//������� ������
			}
			else
				insertPacket(mouseEvent);
		}
		else
			insertClass(mouseEvent);

		break;

	case InsertLine:
		line = new QGraphicsLineItem(QLineF(mouseEvent->scenePos(),
			mouseEvent->scenePos()));
		line->setPen(QPen(myLineColor, 2));
		addItem(line);

		break;

	case InsertText:
		textItem = new DiagramTextItem();
		textItem->setFont(myFont);
		textItem->setTextInteractionFlags(Qt::TextEditorInteraction);
		textItem->setZValue(1000.0);
		connect(textItem, SIGNAL(lostFocus(DiagramTextItem*)),
			this, SLOT(editorLostFocus(DiagramTextItem*)));
		connect(textItem, SIGNAL(selectedChange(QGraphicsItem*)),
			this, SIGNAL(itemSelected(QGraphicsItem*)));
		addItem(textItem);
		textItem->setDefaultTextColor(myTextColor);
		textItem->setPos(mouseEvent->scenePos());
		emit textInserted(textItem);

	case MoveItem:
		QGraphicsItem *item_;	//��������� �� ��������� ������ (QGraphicsItem)
		item_= itemAt(mouseEvent->scenePos());	//���������� ��� ��� ���������
		if(item_ != 0)
		{
			type_ = item_->type();	//���������� ��� ��������
			if(type_ == 65551)	//������ item__ �������� ����� DiagramItem
			{
				DiagramItem * item__;		//��������� �� ��������� ������ (DiagramItem)		
				item__ = dynamic_cast<DiagramItem *>(item_);	//���������� ���� QGraphicsItem � DiagramItem
				if(!item__->getIsParent() )
				{
					item_->setZValue(zValuePacket);
					item__->setZValue(zValuePacket);
					zValuePacket++;
				}
				if(item__->getIsParent() == true)
				{
					item_->setZValue(-9);
					item__->setZValue(-9);
				}
				if(item__->getIsChild())
				{
					item_->setZValue(1);
					item__->setZValue(1);
				}
			}
			if(type_ == 65539)
			{
				// ���������� ��������� ��������� �������
				DiagramTextItem * textItem;
				textItem = dynamic_cast<DiagramTextItem *>(item_);
				QGraphicsItem * itemPacked;
				itemPacked = item_->parentItem();
				DiagramItem * item__;		//��������� �� ��������� ������ (DiagramItem)		
				item__ = dynamic_cast<DiagramItem *>(itemPacked);	//���������� ���� QGraphicsItem � DiagramItem
				QString str;

				str = textItem->toPlainText();

				if(str =="+")
				{
					if(item__->getIsChild() == true)
					{
						textItem->setHtml("-");
						item__->setIsPlus(false);
						QList <Arrow *> list = item__->getListArrows(item__);
						for(int i =0; i<list.count(); i++)
						{
							DiagramItem * itemStart = list.at(i)->startItem();
							DiagramItem * itemEnd = list.at(i)->endItem();
							if(itemStart->parentItem() != itemEnd->parentItem())
							{
								itemStart->removeArrow(list.at(i));
								itemEnd->removeArrow(list.at(i));
								removeItem(list.at(i));
							}

						}
					}
				}
				if(str =="-")
				{
					textItem->setHtml("+");
					item__->setIsPlus(true);
				}
			}
		}

	default:

		;
	}
	QGraphicsScene::mousePressEvent(mouseEvent);
}

void DiagramScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	if (myMode == InsertLine && line != 0) 
	{
		QLineF newLine(line->line().p1(), mouseEvent->scenePos());
		line->setLine(newLine);		
	} 
	else if (myMode == MoveItem) 
	{
		QGraphicsScene::mouseMoveEvent(mouseEvent);
	}
}

void trackingArrow()
{

}

void DiagramScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	if(myMode == MoveItem)
	{
		//������� �������� �������
		trackingArrow();


		QGraphicsItem *item_;	//��������� �� ��������� ������ (QGraphicsItem)
		QGraphicsItem *item_Cros;
		item_= itemAt(mouseEvent->scenePos());	//���������� ��� ��� ���������
		//���� ������ ������
		if(item_ != 0)
		{
			DiagramItem * item__;		//��������� �� ��������� ������ (DiagramItem)		
			item__ = dynamic_cast<DiagramItem *>(item_);	//���������� ���� QGraphicsItem � DiagramItem

			//���� ������ �������� �������
			int type = item_->type();	//���������� ��� ��������
			if(type == 65551)	//������ item__ �������� ����� DiagramItem
			{
				QGraphicsItem *itemChild;
				QGraphicsItem *itemParent;
				item_ = itemAt(mouseEvent->scenePos());	//��������� �� ��������� �����

				banCrossing(item_);

				//���� ����� ��������
				if(item_->parentItem())
				{
					itemChild = item_;
					//item_->setZValue(2);
					itemParent = item_->parentItem();

					//���� �������� ����� ��������� �� ������� ������-��������, �� ������� ��� �������������
					//������� � ������ list ��� ������� ������� ��������� ����� � ������������ ������
					QList<QGraphicsItem *> list = itemParent->collidingItems(Qt::IntersectsItemShape);
					bool haveChild = false;	//������� � ������ �������� ������� � ��������� ������ ��������, ��������� ������
					//������� ���� �������� � ����� �������� �������
					for (int i = 0; i < list.size(); ++i)
					{
						item_ = list.at(i);
						//���� �������� ����� ����� � ������� ������ ��������
						if(item_ == itemChild)
						{
							haveChild = true;							
						}
					}

					//���� �������� ����� �� ����� � ������� ������ ��������
					if(haveChild == false)
					{
						QPointF posItem = itemChild->scenePos();	//���������� ���������� ��������� ������
						itemChild->setParentItem(NULL);	//������ �������� ����� ��������� �������� ������
						itemChild->setPos(posItem);
						DiagramTextItem text_;
						itemChild->setSelected(true);
						DiagramItem * childPacketDiagram;
						childPacketDiagram = dynamic_cast<DiagramItem *>(itemChild);	//���������� ���� QGraphicsItem � DiagramItem
						childPacketDiagram->setIsChild(false);

						QList <QGraphicsItem *> listChild_ = childPacketDiagram->childItems();
						for(int k = 0; k < 100; k++)
						{
							for (int i = 0; i < listChild_.size(); ++i)
							{
								int type_ = listChild_.at(i)->type();	//���������� ��� ��������
								if(type_ == 65539)	//������ item__ �������� ����� DiagramItem
								{
									// ���������� ��������� ��������� �������
									DiagramTextItem * textItem;
									textItem = dynamic_cast<DiagramTextItem *>(listChild_.at(i));
									QString str = textItem->toPlainText();
									if(str =="+" || str =="-")
										textItem->setVisible(false);
								}
							}
						}

						item__ = dynamic_cast<DiagramItem *>(itemParent);	//���������� ���� QGraphicsItem � DiagramItem
						item__->delChildInList(childPacketDiagram);
						//����� �� ���� �������� ������� � �������� �� ������ ���������
						QList <DiagramItem *> listChild = item__->getListChild();
						for(int k = 0; k < 100; k++)
						{
							for (int i = 0; i < listChild.size(); ++i)
							{
								if(listChild.at(i) == itemChild)
								{
									listChild.removeAt(i);
								}
							}
						}
						//���� � ������������� �������� ��� ������ �������� �� ������ � ���� ������ ���������
						if(listChild.size() == 0)
						{
							item__->setIsParent(false);	
							item__->setZValue(0);
							minSizePoligon(item__, 0);
						}
					}
				}
				//���� ����� ���������
				else
				{
					itemChild = item_;
					checkingOutScreen(item__);	//�������� �� ����� �������� �� ������� ������
					if(typeid(item_) == typeid(DiagramItem *))
						item__ = dynamic_cast<DiagramItem *>(item_);	//���������� ���� QGraphicsItem � DiagramItem

					//���� ����� ���������
					if(item__->getIsParent() == false && item__->getIsComment() == false)
					{
						//������ ���������, ������� ������������ � ���� ���������
						QList<QGraphicsItem *> list1 = itemChild->collidingItems(Qt::IntersectsItemShape);
						DiagramItem  * itemParent;
						bool flag1= false;

						if(list1.size() > 3)
						{
							for(int j = list1.size()-1; j > 0; j--)
							{
								int type = list1.at(j)->type();	//���������� ��� ��������
								if(type == 65551)	//������ item__ �������� ����� DiagramItem
								{
									itemParent = dynamic_cast<DiagramItem *>(list1.at(j));	//���������� ���� QGraphicsItem � DiagramItem
									flag1 = true;
									break;
								}
							}
						}
						//����� �������� ��������
						if(flag1 && itemParent->getIsComment()==false)
						{
							//������� ������� ��������� ����������
							DiagramItem *itemChildDiagram = dynamic_cast<DiagramItem *>(itemChild);	//���������� ���� QGraphicsItem � DiagramItem
							QList <QGraphicsItem *> listChild_ = itemChildDiagram->childItems();
							for(int k = 0; k < 100; k++)
							{
								for (int i = 0; i < listChild_.size(); ++i)
								{
									int type_ = listChild_.at(i)->type();	//���������� ��� ��������
									if(type_ == 65539)	//������ item__ �������� ����� DiagramItem
									{
										// ���������� ��������� ��������� �������
										DiagramTextItem * textItem;
										textItem = dynamic_cast<DiagramTextItem *>(listChild_.at(i));
										QString str = textItem->toPlainText();
										if(str =="+" || str =="-")
											textItem->setVisible(true);
									}
								}
							}

							//���� ����� � ������� �������� �������� ����� ��� �� ����� ������� �������
							if(itemParent->getIsParent() == false)
							{
								itemParent->setIsParent(true);	//���������, ��� ����� �������� ������������
								itemParent->setZValue(-9);
								itemParent->addListChild(dynamic_cast<DiagramItem *>(itemChild));	//���������� � ������ ������ �������� ��������� ������
								maxSizePoligon(itemParent, 0);
								itemChild ->setParentItem(itemParent);	
								item__ = dynamic_cast<DiagramItem *>(itemChild);	//���������� ���� QGraphicsItem � DiagramItem
								item__->setIsChild(true);	//���������, ��� ����� �������� ��������
								item__->setZValue(100);
								itemChild ->setPos(10, 50);
							}
							//����� ��� ����� 
							else
							{
								//�������� ���-�� �������� ������
								QList <DiagramItem *> listChild = itemParent->getListChild();
								if(listChild.count() > 2)
								{
									itemChild->moveBy(0,280);
									//������� ��������� �������� �����������
									QList <QGraphicsItem *> listChild_ = itemChild->childItems();
									for(int k = 0; k < 100; k++)
									{
										for (int i = 0; i < listChild_.size(); ++i)
										{
											int type_ = listChild_.at(i)->type();	//���������� ��� ��������
											if(type_ == 65539)	//������ item__ �������� ����� DiagramItem
											{
												// ���������� ��������� ��������� �������
												DiagramTextItem * textItem;
												textItem = dynamic_cast<DiagramTextItem *>(listChild_.at(i));
												QString str = textItem->toPlainText();
												if(str =="+" || str =="-")
													textItem->setVisible(false);
											}
										}
									}
								}
								else
								{
									itemChild ->setParentItem(itemParent);	
									itemParent->addListChild(dynamic_cast<DiagramItem *>(itemChild));	//���������� � ������ ������ �������� ��������� ������
									maxSizePoligon(itemParent, 0);
									itemChild ->setParentItem(itemParent);	
									item__ = dynamic_cast<DiagramItem *>(itemChild);	//���������� ���� QGraphicsItem � DiagramItem
									item__->setIsChild(true);	//���������, ��� ����� �������� ��������
									item__->setZValue(100);
									itemChild ->setPos(165 *listChild.count() , 50);
									QMatrix matrix = itemChild->matrix();
								}
							}

							//������� ��� ����� � ������������
							QGraphicsItem * itemParent = itemChildDiagram->parentItem();
							DiagramItem * itemParentDiagram = dynamic_cast<DiagramItem *>(itemParent);
							QList <Arrow *> listAr = itemChildDiagram->getListArrows(itemChildDiagram);
							for(int i= 0;i<listAr.count();i++)
							{
								Arrow * arrow = listAr.at(i);
								if(arrow->startItem() == itemParentDiagram && arrow->endItem() == itemChildDiagram)
								{						
									itemParentDiagram->removeArrow(arrow);
									itemChildDiagram->removeArrow(arrow);
									removeItem(arrow);
								}
								if(arrow->startItem() == itemChildDiagram && arrow->endItem() == itemParentDiagram)
								{
									itemParentDiagram->removeArrow(arrow);
									itemChildDiagram->removeArrow(arrow);
									removeItem(arrow);
								}
							}
						}
					}
				}

			}
			if(type == 65539)
			{
				QString s;
				item_;
			}

		}
	}

	if (line != 0 && myMode == InsertLine) {
		QList<QGraphicsItem *> startItems = items(line->line().p1());
		if (startItems.count() && startItems.first() == line)
			startItems.removeFirst();
		QList<QGraphicsItem *> endItems = items(line->line().p2());
		if (endItems.count() && endItems.first() == line)
			endItems.removeFirst();

		removeItem(line);
		delete line;



		if (startItems.count() > 0 && endItems.count() > 0 &&
			startItems.first()->type() == DiagramItem::Type &&
			endItems.first()->type() == DiagramItem::Type &&
			startItems.first() != endItems.first()) {


				DiagramItem *startItem =
					qgraphicsitem_cast<DiagramItem *>(startItems.first());

				DiagramItem *endItem =
					qgraphicsitem_cast<DiagramItem *>(endItems.first());

				bool flag = true;
				Arrow *arrow;
				if(startItem->getIsComment() == false && endItem->getIsComment() == false)
				{
					arrow = new Arrow(startItem , endItem);
				}
				if(startItem->getIsComment() == true && endItem->getIsComment() == false)
				{
					arrow = new Arrow(startItem , endItem );
				}
				if(startItem->getIsComment() == false && endItem->getIsComment() == true)
				{
					arrow = new Arrow(endItem,startItem );
				}

				if(startItem->getIsComment() == true && endItem->getIsComment() == true)
				{
					flag = false;
					arrow = new Arrow(endItem,startItem );
				}

				arrow->setColor(myLineColor);
				startItem->addArrow(arrow);




				//����������� �� ��������� ��������� �������
				int size = startItem->returnCountArrow(startItem);
				if (size > 2)
				{
					flag = false;
				}

				//����������� �� ��������� �������� �������
				endItem->addArrow(arrow);
				int size2 = endItem->returnCountArrow(endItem);
				if (size2 > 2)
				{
					flag = false;
				}

				if(endItem->getIsPlus() == false && endItem->getIsChild() ==true)
				{
					if(endItem->parentItem() != startItem->parentItem())
						flag = false;
				}
				else if(startItem->getIsPlus() == false && startItem->getIsChild() ==true)
				{
					if(endItem->parentItem() != startItem->parentItem())
						flag = false;
				}

				if(size>1)
				{			
					for(int i=0; i<size; i++)
					{
						Arrow * ar = startItem->returnStartItem(startItem, i);
						if(ar->endItem() == startItem && ar->startItem() == endItem)
						{
							flag = false;
						}
						size = startItem->returnCountArrow(startItem);
					}
				}

				if(flag)
				{
					if(startItem->getIsChild() || endItem->getIsChild())
					{
						arrow->setZValue(-8.0);
					}
					else
						arrow->setZValue(-10.0);
					addItem(arrow);
					arrow->updatePosition();
				}
				else
				{
					//�������� ������� �� ���������� ������
					size = startItem->returnCountArrow(startItem);
					for(int i=0; i<size; i++)
					{
						Arrow * ar = startItem->returnStartItem(startItem, i);
						if(ar == arrow)
						{
							removeItem(arrow);
							startItem->delArrow(startItem, i);
							break;
						}
					}

					//�������� ������� �� ���������� ������
					size = endItem->returnCountArrow(endItem);
					for(int i=0; i<size; i++)
					{
						Arrow * ar = endItem->returnStartItem(endItem, i);
						if(ar == arrow)
						{
							removeItem(arrow);
							endItem->delArrow(endItem, i);
							break;
						}
					}

				}

		}
	}



	line = 0;
	QGraphicsScene::mouseReleaseEvent(mouseEvent);
}

bool DiagramScene::isItemChange(int type)
{
	foreach (QGraphicsItem *item, selectedItems()) {
		if (item->type() == type)
			return true;
	}
	return false;
}

void DiagramScene::setLineColor(const QColor &color)
{
	myLineColor = color;
	if (isItemChange(Arrow::Type)) {
		Arrow *item =
			qgraphicsitem_cast<Arrow *>(selectedItems().first());
		item->setColor(myLineColor);
		update();
	}
}

void DiagramScene::setTextColor(const QColor &color)
{
	myTextColor = color;
	if (isItemChange(DiagramTextItem::Type)) {
		DiagramTextItem *item =
			qgraphicsitem_cast<DiagramTextItem *>(selectedItems().first());
		item->setDefaultTextColor(myTextColor);
	}
}

void DiagramScene::setItemColor(const QColor &color)
{
	myItemColor = color;
	if (isItemChange(DiagramItem::Type)) {
		DiagramItem *item =
			qgraphicsitem_cast<DiagramItem *>(selectedItems().first());
		item->setBrush(myItemColor);
	}
}

void DiagramScene::setFont(const QFont &font)
{
	myFont = font;

	if (isItemChange(DiagramTextItem::Type)) {
		QGraphicsTextItem *item =
			qgraphicsitem_cast<DiagramTextItem *>(selectedItems().first());
		//At this point the selection can change so the first selected item might not be a DiagramTextItem
		if (item)
			item->setFont(myFont);
	}
}

void DiagramScene::insertPacket(QGraphicsSceneMouseEvent *mouseEvent,bool isInsertInPacket)
{

	//�������� ������
	DiagramItem *item;
	item = new DiagramItem(myItemType, myItemMenu);
	editCountPacket(1);
	if(getCountPacket()>50)
		return;
	item->setBrush(myItemColor);
	item->setId(maxId++);
	item->setIsChild(false);
	item->setIsParent(false);
	item->setIsPlus(true);
	item->setZValue(zValuePacket++);
	item->setIsComment(false);
	int k = item->returnId();
	//����� �������� ������

	//������� ������� ��� ���������� ����������\���������� �������
	textPlus = new DiagramTextItem();
	textPlus->setFont(myFont);
	textPlus->setEnabled(false);
	textPlus->setZValue(1000.0);
	textPlus->setDefaultTextColor(myTextColor);
	textPlus->setParentItem(item);	
	textPlus->setPos(-70,-75);
	textPlus->setHtml("+");
	textPlus->ungrabMouse(); //����������� ����� ����� ����
	textPlus->setTextInteractionFlags(Qt::NoTextInteraction);
	textPlus->setTextWidth(1);
	//����� ������� ������� ��� ���������� ����������\���������� �������
	DiagramItem * item__;		//��������� �� ��������� ������ (DiagramItem)		
	item__ = dynamic_cast<DiagramItem *>(item);	//���������� ���� QGraphicsItem � DiagramItem
	maxSizePoligon(item__, 0);
	minSizePoligon(item__, 0);

	if(isInsertInPacket)
	{
		QGraphicsItem *itemChild_ = item;
		QGraphicsItem *itemParent_;	//��������� �� ����� ��������
		itemParent_ = itemAt(mouseEvent->scenePos());	//���������� ����� ��������

		DiagramItem *itemParent = dynamic_cast<DiagramItem *>(itemParent_);
		DiagramItem *itemChild = dynamic_cast<DiagramItem *>(itemChild_);

		int type = itemParent_->type();
		if(type == 65551 && itemParent->getIsComment() == false && itemParent->getIsChild() == false)	//������� � ������� ���� ������� ������ �� �������� ������������
		{
			if(itemParent_->parentItem())
				itemParent_ = itemParent_->parentItem();

			//���� ����� � ������� �������� �������� ����� ��� �� ����� ������� �������
			if(itemParent->getIsParent() == false)
			{
				itemParent->setIsParent(true);	//���������, ��� ����� �������� ������������
				itemParent->setZValue(-9);
				itemParent->addListChild(dynamic_cast<DiagramItem *>(itemChild));	//���������� � ������ ������ �������� ��������� ������
				maxSizePoligon(itemParent, 0);
				itemChild ->setParentItem(itemParent);	
				itemChild->setIsChild(true);	//���������, ��� ����� �������� ��������
				itemChild->setZValue(100);
				itemChild ->setPos(10, 50);
			}
			//����� ��� ����� 
			else
			{
				//�������� ���-�� �������� ������
				QList <DiagramItem *> listChild = itemParent->getListChild();
				if(listChild.count() > 2)
				{
					itemChild->setPos(mouseEvent->scenePos());
					itemChild->moveBy(0,280);
				}
				else
				{
					itemChild ->setParentItem(itemParent);	
					itemParent->addListChild(dynamic_cast<DiagramItem *>(itemChild));	//���������� � ������ ������ �������� ��������� ������
					itemChild->setIsChild(true);	//���������, ��� ����� �������� ��������
					itemChild->setZValue(100);
					itemChild ->setPos(165 *listChild.count() , 50);
					QMatrix matrix = itemChild->matrix();
				}
			}
		}
	}


	//������� �������� ������
	textNameItem = new DiagramTextItem();
	textNameItem->setFont(myFont);
	textNameItem->setTextInteractionFlags(Qt::TextEditorInteraction);
	textNameItem->setZValue(1000.0);
	connect(textNameItem, SIGNAL(lostFocus(DiagramTextItem*)), this, SLOT(editorLostFocusName(DiagramTextItem*)));
	connect(textNameItem, SIGNAL(selectedChange(QGraphicsItem*)), this, SIGNAL(itemSelected(QGraphicsItem*)));
	textNameItem->setDefaultTextColor(myTextColor);
	textNameItem->setParentItem(item);	
	textNameItem->setPos(-60,-75);
	QString s;
	s.setNum(countPacket);
	textNameItem->setHtml("����� ����� (" + s +")");
	textNameItem->ungrabMouse(); //����������� ����� ����� ����
	textNameItem->setTextInteractionFlags(Qt::TextEditable);
	textNameItem->setTextWidth(100);
	//����� ������� �������� ������

	//������� ������ � �����
	textInItem = new DiagramTextItem();
	textInItem->setFont(myFont);
	textInItem->setTextInteractionFlags(Qt::TextEditorInteraction);
	textInItem->setZValue(1000.0);
	connect(textInItem, SIGNAL(lostFocus(DiagramTextItem*)), this, SLOT(editorLostFocusDescription(DiagramTextItem*)));
	connect(textInItem, SIGNAL(selectedChange(QGraphicsItem*)), this, SIGNAL(itemSelected(QGraphicsItem*)));
	textInItem->setDefaultTextColor(myTextColor);
	textInItem->setParentItem(item);	
	textInItem->setPos(-70,-50);
	textInItem->setHtml("�������� ������");
	textInItem->ungrabMouse(); //����������� ����� ����� ����
	textInItem->setTextInteractionFlags(Qt::TextEditable);
	textInItem->setTextWidth(150);
	//����� ������� ������ � �����

	emit itemInserted(item);
}

void DiagramScene::insertPacket(QGraphicsSceneMouseEvent *mouseEvent)
{

	//�������� ������
	DiagramItem *item;
	item = new DiagramItem(myItemType, myItemMenu);
	editCountPacket(1);
	if(getCountPacket()>50)
		return;
	item->setBrush(myItemColor);
	addItem(item);
	item->setPos(mouseEvent->scenePos());
	item->setId(maxId++);
	item->setIsChild(false);
	item->setIsParent(false);
	/*add*/	item->setIsPlus(true);
	item->setIsComment(false);
	item->setZValue(zValuePacket++);
	//����� �������� ������

	//������� ������� ��� ���������� ����������\���������� �������
	textPlus = new DiagramTextItem();
	textPlus->setFont(myFont);
	textPlus->setEnabled(false);
	textPlus->setZValue(1000.0);
	textPlus->setDefaultTextColor(myTextColor);
	textPlus->setParentItem(item);	
	textPlus->setPos(-70,-75);
	textPlus->setHtml("+");
	textPlus->ungrabMouse(); //����������� ����� ����� ����
	textPlus->setTextInteractionFlags(Qt::NoTextInteraction);
	textPlus->setTextWidth(1);
	textPlus->setVisible(false);
	//����� ������� ������� ��� ���������� ����������\���������� �������


	//������� �������� ������
	textNameItem = new DiagramTextItem();
	textNameItem->setFont(myFont);
	textNameItem->setTextInteractionFlags(Qt::TextEditorInteraction);
	textNameItem->setZValue(1000.0);
	connect(textNameItem, SIGNAL(lostFocus(DiagramTextItem*)), this, SLOT(editorLostFocusName(DiagramTextItem*)));
	connect(textNameItem, SIGNAL(selectedChange(QGraphicsItem*)), this, SIGNAL(itemSelected(QGraphicsItem*)));
	textNameItem->setDefaultTextColor(myTextColor);
	textNameItem->setParentItem(item);	
	textNameItem->setPos(-60,-75);
	QString s;
	s.setNum(countPacket);
	textNameItem->setHtml("����� ����� (" + s +")");
	textNameItem->ungrabMouse(); //����������� ����� ����� ����
	textNameItem->setTextInteractionFlags(Qt::TextEditable);
	textNameItem->setTextWidth(100);
	//����� ������� �������� ������

	//������� ������ � �����
	textInItem = new DiagramTextItem();
	textInItem->setFont(myFont);
	textInItem->setTextInteractionFlags(Qt::TextEditorInteraction);
	textInItem->setZValue(1000.0);
	connect(textInItem, SIGNAL(lostFocus(DiagramTextItem*)), this, SLOT(editorLostFocusDescription(DiagramTextItem*)));
	connect(textInItem, SIGNAL(selectedChange(QGraphicsItem*)), this, SIGNAL(itemSelected(QGraphicsItem*)));
	textInItem->setDefaultTextColor(myTextColor);
	textInItem->setParentItem(item);	
	textInItem->setPos(-70,-50);
	textInItem->setHtml("�������� ������");
	textInItem->ungrabMouse(); //����������� ����� ����� ����
	textInItem->setTextInteractionFlags(Qt::TextEditable);
	textInItem->setTextWidth(150);
	//����� ������� ������ � �����

	DiagramItem * item__;		//��������� �� ��������� ������ (DiagramItem)		
	item__ = dynamic_cast<DiagramItem *>(item);	//���������� ���� QGraphicsItem � DiagramItem
	maxSizePoligon(item__, 0);
	minSizePoligon(item__, 0);

	//DiagramItem * item2 = getItemId(item__, 1);

	emit itemInserted(item);
}

void DiagramScene::insertPacket(int x, int y, bool isInsertInPacket, int idPacket, int idParentPacket, int isPlus)
{

	//�������� ������
	DiagramItem *item;
	item = new DiagramItem(myItemType, myItemMenu);
	editCountPacket(1);
	if(getCountPacket()>50)
		return;
	item->setBrush(myItemColor);
	item->setId(idPacket);
	item->setIsChild(false);
	item->setIsParent(false);
	if(isPlus == 1)
	{
		item->setIsPlus(true);
	}
	else
	{
		item->setIsPlus(false);
	}
	item->setZValue(zValuePacket++);
	item->setIsComment(false);
	int k = item->returnId();
	//����� �������� ������

	//������� ������� ��� ���������� ����������\���������� �������
	textPlus = new DiagramTextItem();
	textPlus->setFont(myFont);
	textPlus->setEnabled(false);
	textPlus->setZValue(1000.0);
	textPlus->setDefaultTextColor(myTextColor);
	textPlus->setParentItem(item);	
	textPlus->setPos(-70,-75);
	if(isPlus == 1)
	{
		textPlus->setHtml("+");
	}
	else
	{
		textPlus->setHtml("-");
	}
	textPlus->ungrabMouse(); //����������� ����� ����� ����
	textPlus->setTextInteractionFlags(Qt::NoTextInteraction);
	textPlus->setTextWidth(1);
	//����� ������� ������� ��� ���������� ����������\���������� �������
	
	DiagramItem * item__;		//��������� �� ��������� ������ (DiagramItem)		
	item__ = dynamic_cast<DiagramItem *>(item);	//���������� ���� QGraphicsItem � DiagramItem
	maxSizePoligon(item__, 0);
	minSizePoligon(item__, 0);

	if(isInsertInPacket)
	{
		QGraphicsItem *itemChild_ = item;
		DiagramItem * itemtemp = new DiagramItem(myItemType, myItemMenu);
		addItem(itemtemp);
		
		DiagramItem * itemParent = itemtemp->getItemId(itemtemp, idParentPacket);//���������� ����� ��������
		removeItem(itemtemp);

		DiagramItem *itemChild = dynamic_cast<DiagramItem *>(itemChild_);
		int a = itemParent->x();
		int b = itemParent->y();
		//���� ����� � ������� �������� �������� ����� ��� �� ����� �������� �������
		if(itemParent->getIsParent() == false)
		{
			itemParent->setIsParent(true);	//���������, ��� ����� �������� ������������
			itemParent->setZValue(-9);
			itemParent->addListChild(dynamic_cast<DiagramItem *>(itemChild));	//���������� � ������ ������ �������� ��������� ������
			maxSizePoligon(itemParent, 0);
			itemChild ->setParentItem(itemParent);	
			itemChild->setIsChild(true);	//���������, ��� ����� �������� ��������
			itemChild->setZValue(100);
			itemChild ->setPos(x-a, y-b);
		}
		//����� ��� ����� 
		else
		{
			//�������� ���-�� �������� ������
			QList <DiagramItem *> listChild = itemParent->getListChild();
			if(listChild.count() > 2)
			{
				itemChild->setPos(x-a, y-b);
				//itemChild->moveBy(0,280);
			}
			else
			{
				itemChild ->setParentItem(itemParent);	
				itemParent->addListChild(dynamic_cast<DiagramItem *>(itemChild));	//���������� � ������ ������ �������� ��������� ������
				itemChild->setIsChild(true);	//���������, ��� ����� �������� ��������
				itemChild->setZValue(100);
				itemChild ->setPos(x-a, y-b);
				QMatrix matrix = itemChild->matrix();
			}
		}
	}
	else
	{
	}
	emit itemInserted(item);
}

void DiagramScene::insertPacket(int x, int y, int idPacket, int isPlus)
{

	//�������� ������
	DiagramItem *item;
	item = new DiagramItem(myItemType, myItemMenu);
	editCountPacket(1);
	if(getCountPacket()>50)
		return;
	item->setBrush(myItemColor);
	addItem(item);
	item->setPos(x, y);
	item->setId(idPacket);
	item->setIsChild(false);
	item->setIsParent(false);
	if(isPlus == 1)
	{
		item->setIsPlus(true);
	}
	else
	{
		item->setIsPlus(false);
	}
	item->setIsComment(false);
	item->setZValue(zValuePacket++);
	//����� �������� ������
	
	DiagramItem * item__;		//��������� �� ��������� ������ (DiagramItem)		
	item__ = dynamic_cast<DiagramItem *>(item);	//���������� ���� QGraphicsItem � DiagramItem
	maxSizePoligon(item__, 0);
	minSizePoligon(item__, 0);

	//������� ������� ��� ���������� ����������\���������� �������
	textPlus = new DiagramTextItem();
	textPlus->setFont(myFont);
	textPlus->setEnabled(false);
	textPlus->setZValue(1000.0);
	textPlus->setDefaultTextColor(myTextColor);
	textPlus->setParentItem(item);	
	textPlus->setPos(-70,-75);
	if(isPlus == 1)
	{
		textPlus->setHtml("+");
	}
	else
	{
		textPlus->setHtml("-");
	}
	textPlus->ungrabMouse(); //����������� ����� ����� ����
	textPlus->setTextInteractionFlags(Qt::NoTextInteraction);
	textPlus->setTextWidth(1);
	//����� ������� ������� ��� ���������� ����������\���������� �������
	emit itemInserted(item);
}

void DiagramScene::insertClass(QGraphicsSceneMouseEvent *mouseEvent)
{

	//�������� ������
	DiagramItem *item;
	item = new DiagramItem(myItemType, myItemMenu);
	editCountPacket(1);
	if(getCountPacket()>50)
		return;
	item->setBrush(myItemColor);
	addItem(item);
	item->setPos(mouseEvent->scenePos());
	item->setId(maxId++);
	item->setIsChild(false);
	item->setIsParent(false);
	item->setIsComment(true);
	item->setZValue(zValuePacket++);
	//����� �������� ������

	//������� �������� ����������
	textNameItem = new DiagramTextItem();
	textNameItem->setFont(myFont);
	textNameItem->setTextInteractionFlags(Qt::TextEditorInteraction);
	textNameItem->setZValue(1000.0);
	connect(textNameItem, SIGNAL(lostFocus(DiagramTextItem*)), this, SLOT(editorLostClass(DiagramTextItem*)));
	connect(textNameItem, SIGNAL(selectedChange(QGraphicsItem*)), this, SIGNAL(itemSelected(QGraphicsItem*)));
	textNameItem->setDefaultTextColor(myTextColor);
	textNameItem->setParentItem(item);	
	textNameItem->setPos(-60,-40);
	QString s;
	s.setNum(countPacket);
	textNameItem->setHtml("����� �����������");
	textNameItem->ungrabMouse(); //����������� ����� ����� ����
	textNameItem->setTextInteractionFlags(Qt::TextEditable);
	textNameItem->setTextWidth(100);
	//����� ������� �������� �����������

	emit itemInserted(item);
}

void DiagramScene::insertClass(int x,  int y)
{

	//�������� ������
	DiagramItem *item;
	item = new DiagramItem(myItemType, myItemMenu);
	editCountPacket(1);
	if(getCountPacket()>50)
		return;
	item->setBrush(myItemColor);
	addItem(item);
	item->setPos(x, y);
	item->setId(maxId++);
	item->setIsChild(false);
	item->setIsParent(false);
	item->setIsComment(true);
	item->setZValue(zValuePacket++);
	//����� �������� ������

	emit itemInserted(item);
}

void DiagramScene::insertText(int x, int y, QString ref, int w, int s, int c)
{
	DiagramTextItem *textItem = new DiagramTextItem();
	textItem->setTextInteractionFlags(Qt::TextEditorInteraction);
	textItem->setTextWidth(w);
	addItem(textItem);
	QPoint pos(x,y);
	textItem->setPlainText(ref);
	textItem->setPos(pos);
	textItem->setZValue(100);
	emit textInserted(textItem);
}


//���������� ������� �������� ��� ���������� � ���� ��������� ������
void DiagramScene::maxSizePoligon(DiagramItem *item, int countChildrenItems)
{
	QPolygonF newPoligon;
	newPoligon	<< QPointF(-70, -50) << QPointF(500, -50)
		<< QPointF(500, 210) << QPointF(-70, 210)
		<< QPointF(-70, -80) << QPointF(35, -80)
		<< QPointF(35, -50);
	item->setPolygon(newPoligon);

	item->resizePoligonMax();	//������������ ���������
}

//���������� ������� �������� ��� �������� �� ���� ��������� ������
void DiagramScene::minSizePoligon(DiagramItem *item, int countChildrenItems)
{
	QPolygonF newPoligon;
	newPoligon	<< QPointF(-70, -50) << QPointF(80, -50)
		<< QPointF(80, 50) << QPointF(-70, 50)
		<< QPointF(-70, -80) << QPointF(35, -80)
		<< QPointF(35, -50);
	item->setPolygon(newPoligon);
	item->resizePoligonMin();	//������������ ���������
}

//�������� ������ �� �������
void DiagramScene::checkingOutScreen(DiagramItem *item)
{	
	QPointF pointLeft;	//������� ����� ����� ������
	pointLeft = item->mapToScene(item->polygon()[4]);
	QPointF pointRight;	//������ ������ ����� ������
	pointRight = item->mapToScene(item->polygon()[2]);

	//�������� �� ����� �� ������� ������� ����� ����� ������
	//����� �� ������ �������
	if(pointRight.x() > 5000)
		item->moveBy(-400,0);	
	//����� �� ������� �������
	if(pointLeft.y() < 30)
		item->moveBy(0,400);
	//����� �� ����� �������
	if(pointLeft.x()<0)
		item->moveBy(400,0);
	//����� �� ������ �������
	if(pointRight.y() > 5000)
		item->moveBy(0,-400);
}

//�������� ������
void DiagramScene::removePacket(QGraphicsItem * item_)
{
	DiagramItem * item__;		//��������� �� ��������� ������ (DiagramItem)		
	item__ = dynamic_cast<DiagramItem *>(item_);	//���������� ���� QGraphicsItem � DiagramItem

	//���� ������ �������� �������
	int type = item_->type();	//���������� ��� ��������
	if(type == 65551)	//������ item__ �������� ����� DiagramItem
	{
		QGraphicsItem *itemChild;
		QGraphicsItem *itemParent;

		//���� ����� ��������
		if(item_->parentItem())
		{
			itemChild = item_;
			itemParent = item_->parentItem();

			QPointF posItem = itemChild->scenePos();	//���������� ���������� ��������� ������
			itemChild->setParentItem(NULL);	//������ �������� ����� ��������� �������� ������
			itemChild->setPos(posItem);
			DiagramTextItem text_;
			itemChild->setSelected(true);

			DiagramItem * childPacketDiagram;

			childPacketDiagram = dynamic_cast<DiagramItem *>(itemChild);	//���������� ���� QGraphicsItem � DiagramItem
			childPacketDiagram->setIsChild(false);

			item__ = dynamic_cast<DiagramItem *>(itemParent);	//���������� ���� QGraphicsItem � DiagramItem
			item__->delChildInList(childPacketDiagram);

			//����� �� ���� �������� ������� � �������� �� ������ ���������
			QList <DiagramItem *> listChild = item__->getListChild();
			for(int k = 0; k < 50; k++)
			{
				for (int i = 0; i < listChild.size(); ++i)
				{
					if(listChild.at(i) == itemChild)
					{
						listChild.removeAt(i);
					}
				}
			}
			//���� � ������������� �������� ��� ������ �������� �� ������ � ���� ������ ���������
			if(listChild.size() == 0)
			{
				item__->setIsParent(false);	
				item__->setZValue(0);
				minSizePoligon(item__, 0);					
			}
		}
		//���� ����� ��� ������������
		if(item__->getIsParent())
		{
			QList <DiagramItem *> list1 = item__->getListChild();
			for(int i=0;i<list1.count();i++)
			{
				QList <Arrow *> list = list1.at(i)->getListArrows(list1.at(i));
				for(int j =0; j<list.count(); j++)
				{
					DiagramItem * itemStart = list.at(j)->startItem();
					DiagramItem * itemEnd = list.at(j)->endItem();
					itemStart->removeArrow(list.at(j));
					itemEnd->removeArrow(list.at(j));
					removeItem(list.at(j));
				}
			}
		}
	}
}

void DiagramScene::delArrow(Arrow * ar)
{

}

//����������� ������� ���� �� �����
void DiagramScene::banCrossing(QGraphicsItem * itemMainGraphics)
{
	DiagramItem * itemMainDiagram =  dynamic_cast<DiagramItem *>(itemMainGraphics);	//���������� ���� QGraphicsItem � DiagramItem
	if(itemMainDiagram->getIsComment() == true)
	{
		//� ������ ���������� ��� ������� ������������
		QList <QGraphicsItem *> listCrossing = itemMainGraphics->collidingItems(Qt::IntersectsItemBoundingRect);

		for(int i = 0; i<listCrossing.count();i++)
		{
			QGraphicsItem * itemCrossGraphics = listCrossing.at(i);
			DiagramItem * itemCrossDiagram = dynamic_cast<DiagramItem *>(itemCrossGraphics);	//���������� ���� QGraphicsItem � DiagramItem

			// ���� ������ �������� �������
			int type = itemCrossGraphics->type();	//���������� ��� ��������
			if(type == 65551)	//������ item__ �������� ����� DiagramItem
			{
				int dx = 50;
				int dy = 50;
				bool cross = itemMainGraphics->collidesWithItem(itemCrossGraphics, Qt::IntersectsItemBoundingRect);
				while (cross)
				{
					itemMainGraphics->moveBy(dx,0);
					cross = itemMainGraphics->collidesWithItem(itemCrossGraphics, Qt::IntersectsItemBoundingRect);
				}
				banCrossing(itemMainGraphics);
			}
		}
	}
}