#include "mainwindow.h"
//#include <QXmlStreamReader>
//#include <QGraphicsItem>
//#include "diagramtextitem.h"

const int InsertTextButton = 10;

MainWindow::MainWindow(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);
	readSettings();
	createActions();//�������� ���� ������, � ���������� �������
	createToolBox();//�� ���� ��� �� ��� �������� ���� �� �������!
	createMenus();//�������� �������� ����

	scene = new DiagramScene(itemMenu, this);
	scene->setSceneRect(QRectF(0, 0, 5000, 5000));//�������� Diagramsscene �������� 5000�5000 ��������
	createToolbars();//�������� ������ ����� � ���������� ��������� �� ���

	//������� ��������� ��������
	connect(scene, SIGNAL(itemInserted(DiagramItem*)), this, SLOT(itemInserted(DiagramItem*)));
	connect(scene, SIGNAL(textInserted(QGraphicsTextItem*)), this, SLOT(textInserted(QGraphicsTextItem*)));
	connect(scene, SIGNAL(itemSelected(QGraphicsItem*)), this, SLOT(itemSelected(QGraphicsItem*)));
	connect(scene, SIGNAL(selectionChanged()), this, SLOT(item_Selected()));
	//connect(scene, SIGNAL(lineInserted(QGraphicsLineItem*)), this, SLOT(lineInserted(QGraphicsLineItem*)));

	//������� ���������� ��������
	connect(ui.actionSetItem, SIGNAL(triggered()), this, SLOT(setItemInsert()));
	connect(ui.actionSetLine, SIGNAL(triggered()), this, SLOT(setLineInsert()));
	connect(ui.actionSetText, SIGNAL(triggered()), this, SLOT(setTextInsert()));

	//�������� ��������� �������� ���������
	connect(ui.action1, SIGNAL(triggered()), this, SLOT(loadone()));
	connect(ui.action2, SIGNAL(triggered()), this, SLOT(loadtwo()));
	connect(ui.action3, SIGNAL(triggered()), this, SLOT(loadthree()));


	//������� �������� ��������(��)
	connect(ui.actionDelite, SIGNAL(triggered()), this, SLOT(deleteItem()));
	connect(ui.clearScene, SIGNAL(triggered()), this, SLOT(clearItem()));

	//������� ���������� � �������� ���������
	connect(ui.actionSave, SIGNAL(triggered()), this, SLOT(sl_saveSceneToFile()));
	connect(ui.actionLoad, SIGNAL(triggered()), this, SLOT(sl_loadSceneFromFile()));
	connect(ui.actionExport, SIGNAL(triggered()), this, SLOT(export_picture()));
	connect(ui.print, SIGNAL(triggered()), this, SLOT(print()));//���������� �� ������



	//���������� layout ��� ���������� ��������
	QHBoxLayout *layout = new QHBoxLayout;
	view = new QGraphicsView(scene); //����������� ��������� QGrophicsscene

	ui.layout->addWidget(view);

	QWidget *widget = new QWidget;
	widget->setLayout(ui.layout);

	setCentralWidget(widget);
	setWindowTitle(tr("Digrafer"));
	setUnifiedTitleAndToolBarOnMac(true);
}

MainWindow::~MainWindow()
{

}
void MainWindow::closeEvent(QCloseEvent *event)
{
	QMessageBox msgBox;
	msgBox.setText("� ������� ���� ������� ���������");
	msgBox.setInformativeText("������ �� �� ��������� ���������?");
	msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Save);
	int ret = msgBox.exec();
	switch (ret) {
	case QMessageBox::Save:
		// Save was clicked
		sl_saveSceneToFile();
		close();
		writeSettings();
		event->accept();
		break;
	case QMessageBox::Discard:
		// Don't Save was clicked
		close();
		writeSettings();
		event->accept();
		break;
	case QMessageBox::Cancel:
		// Cancel was clicked
		event->ignore();
		break;
	default:
		// should never be reached
		break;
	}
}
void MainWindow::readSettings()
{
	this->restoreGeometry(settings.value("geometry").toByteArray());
	restoreState(settings.value("windowState").toByteArray());	
	fileName=settings.value("saveFileName").toString();
}

void MainWindow::writeSettings()
{   
	settings.setValue("geometry", saveGeometry());
	settings.setValue("windowState", saveState());
}

//������� "�� �������� ���"
void MainWindow::bringToFront()
{
	if (scene->selectedItems().isEmpty())
		return;

	QGraphicsItem *selectedItem = scene->selectedItems().first();
	QList<QGraphicsItem *> overlapItems = selectedItem->collidingItems();

	qreal zValue = 0;
	foreach (QGraphicsItem *item, overlapItems) {
		if (item->zValue() >= zValue &&
			item->type() == DiagramItem::Type)
			zValue = item->zValue() + 0.1;
	}
	selectedItem->setZValue(zValue);
}

//������� "�� ������ ���"
void MainWindow::sendToBack()
{
	if (scene->selectedItems().isEmpty())
		return;

	QGraphicsItem *selectedItem = scene->selectedItems().first();
	QList<QGraphicsItem *> overlapItems = selectedItem->collidingItems();

	qreal zValue = 0;
	foreach (QGraphicsItem *item, overlapItems) {
		if (item->zValue() <= zValue &&
			item->type() == DiagramItem::Type)
			zValue = item->zValue() - 0.1;
	}
	selectedItem->setZValue(zValue);
}

//������� ��������� ������ ������
void MainWindow::currentFontChanged(const QFont &)
{
	handleFontChange();
}

//������� ��������� ������ ������
void MainWindow::fontSizeChanged(const QString &)
{
	handleFontChange();
}

//������� ��������� ������� �����
void MainWindow::sceneScaleChanged(const QString &scale)
{
	double newScale = scale.left(scale.indexOf(tr("%"))).toDouble() / 100.0;
	QMatrix oldMatrix = view->matrix();
	view->resetMatrix();
	view->translate(oldMatrix.dx(), oldMatrix.dy());
	view->scale(newScale, newScale);
}

//������� ��������� ����� ������
void MainWindow::textColorChanged()
{
	textAction = qobject_cast<QAction *>(sender());
	fontColorToolButton->setIcon(createColorToolButtonIcon(
		".\\images\\textpointer.png",
		qvariant_cast<QColor>(textAction->data())));
	textButtonTriggered();
}

//������� ��������� ����� ��������(������)
//���������� �� 2 ������
void MainWindow::itemColorChanged()
{
	fillAction = qobject_cast<QAction *>(sender());
	fillColorToolButton->setIcon(createColorToolButtonIcon(
		".\\images\\floodfill.png",
		qvariant_cast<QColor>(fillAction->data())));
	fillButtonTriggered();
}

//������� ��������� ����� ������
void MainWindow::lineColorChanged()
{
	lineAction = qobject_cast<QAction *>(sender());
	lineColorToolButton->setIcon(createColorToolButtonIcon(
		".\\images\\linecolor.png",
		qvariant_cast<QColor>(lineAction->data())));
	lineButtonTriggered();
}

//������ ��������� ����� ������
void MainWindow::textButtonTriggered()
{
	scene->setTextColor(qvariant_cast<QColor>(textAction->data()));
}

//������ ��������� ����� ��������
void MainWindow::fillButtonTriggered()
{
	scene->setItemColor(qvariant_cast<QColor>(fillAction->data()));
}

//������ ��������� ����� �����
void MainWindow::lineButtonTriggered()
{
	scene->setLineColor(qvariant_cast<QColor>(lineAction->data()));
}

//������� ��������� ����� ������(������)
void MainWindow::handleFontChange()
{
	QFont font = fontCombo->currentFont();
	font.setPointSize(fontSizeCombo->currentText().toInt());
	font.setWeight(boldAction->isChecked() ? QFont::Bold : QFont::Normal);
	font.setItalic(italicAction->isChecked());
	font.setUnderline(underlineAction->isChecked());

	scene->setFont(font);
}

//������� ������� ������ �����
void MainWindow::createToolBox()
{
	buttonGroup = new QButtonGroup(this);
	buttonGroup->setExclusive(false);
	connect(buttonGroup, SIGNAL(buttonClicked(int)),
		this, SLOT(buttonGroupClicked(int)));
	QGridLayout *layout = new QGridLayout;

	QToolButton *textButton = new QToolButton;
	textButton->setCheckable(true);
	buttonGroup->addButton(textButton, InsertTextButton);
	textButton->setIcon(QIcon(QPixmap(".\\images\\textpointer.png")
		.scaled(30, 30)));
	textButton->setIconSize(QSize(50, 50));
	QGridLayout *textLayout = new QGridLayout;
	textLayout->addWidget(textButton, 0, 0, Qt::AlignHCenter);
	textLayout->addWidget(new QLabel(tr("Text")), 1, 0, Qt::AlignCenter);
	QWidget *textWidget = new QWidget;
	textWidget->setLayout(textLayout);
	layout->addWidget(textWidget, 1, 1);

	layout->setRowStretch(3, 10);
	layout->setColumnStretch(2, 10);

	QWidget *itemWidget = new QWidget;
	itemWidget->setLayout(layout);

	backgroundButtonGroup = new QButtonGroup(this);
	connect(backgroundButtonGroup, SIGNAL(buttonClicked(QAbstractButton*)),
		this, SLOT(backgroundButtonGroupClicked(QAbstractButton*)));

}

//������� ������� ���� 
void MainWindow::createMenus()
{
	/* itemMenu = menuBar()->addMenu(tr("&Item"));
	itemMenu->addAction(deleteAction);
	itemMenu->addSeparator();
	itemMenu->addAction(toFrontAction);
	itemMenu->addAction(sendBackAction);*/

	helpMenu = menuBar()->addMenu(tr("������"));
	helpMenu->addAction(help);
	//� ��������� � ������������� �������������
	aboutMenu = menuBar()->addMenu(tr("� ���������"));
	aboutMenu->addAction(aboutActionPr);
}

//������� �������� ���� � ������ ������������
void MainWindow::createActions()
{
	toFrontAction = new QAction(QIcon(".\\images\\bringtofront.png"),
		tr("Bring to &Front"), this);
	toFrontAction->setShortcut(tr("Ctrl+F"));
	toFrontAction->setStatusTip(tr("Bring item to front"));
	connect(toFrontAction, SIGNAL(triggered()),
		this, SLOT(bringToFront()));

	clearAll = new QAction(QIcon(".\\images\\Clear.png"),
		tr("&Clear"), this);
	clearAll->setShortcut(tr("Ctrl+Delete"));
	clearAll->setStatusTip(tr("Delete diagram"));
	connect(clearAll, SIGNAL(triggered()),
		this, SLOT(clearItem()));


	//������
	ui.print->setShortcut(tr("Ctrl+P"));
	//����������
	ui.actionSave->setShortcut(tr("Ctrl+S"));
	//load
	ui.actionLoad->setShortcut(tr("Ctrl+L"));
	//export to pic
	ui.actionExport->setShortcut(tr("Ctrl+E"));

	//add elements
	ui.actionSetItem->setShortcut(tr("Ctrl+T"));
	ui.actionSetLine->setShortcut(tr("Ctrl+D"));
	ui.actionSetText->setShortcut(tr("Ctrl+M"));

	sendBackAction = new QAction(QIcon(".\\images\\sendtoback.png"),
		tr("Send to &Back"), this);
	sendBackAction->setShortcut(tr("Ctrl+B"));
	sendBackAction->setStatusTip(tr("Send item to back"));
	connect(sendBackAction, SIGNAL(triggered()),
		this, SLOT(sendToBack()));

	deleteAction = new QAction(QIcon(".\\images\\delete.png"),
		tr("&Delete"), this);
	deleteAction->setShortcut(tr("Delete"));
	deleteAction->setStatusTip(tr("Delete item from diagram"));
	connect(deleteAction, SIGNAL(triggered()),
		this, SLOT(deleteItem()));

	boldAction = new QAction(tr("Bold"), this);
	boldAction->setCheckable(true);
	QPixmap pixmap(".\\images\\bold.png");
	boldAction->setIcon(QIcon(pixmap));
	boldAction->setShortcut(tr("Ctrl+W"));
	connect(boldAction, SIGNAL(triggered()),
		this, SLOT(handleFontChange()));

	italicAction = new QAction(QIcon(".\\images\\italic.png"),
		tr("Italic"), this);
	italicAction->setCheckable(true);
	italicAction->setShortcut(tr("Ctrl+I"));
	connect(italicAction, SIGNAL(triggered()),
		this, SLOT(handleFontChange()));

	underlineAction = new QAction(QIcon(".\\images\\underline.png"),
		tr("Underline"), this);
	underlineAction->setCheckable(true);
	underlineAction->setShortcut(tr("Ctrl+U"));
	connect(underlineAction, SIGNAL(triggered()),
		this, SLOT(handleFontChange()));

	//������� ������� � ���������
	aboutActionPr = new QAction(tr("� ���������"), this);
	aboutActionPr->setShortcut(tr("Ctrl+O"));
	connect(aboutActionPr, SIGNAL(triggered()),this, SLOT(aboutProj()));
	//����� �����
	help=new QAction("Help",this);
	help->setShortcut(tr("Ctrl+H"));
	connect(help,SIGNAL(triggered()),this,SLOT(Help()));
}

//�������� ������ ����������������� � ���������� ������ �� ���
void MainWindow::createToolbars()
{
	//�������� ���� �������������� �����
	editToolBar = addToolBar(tr("Edit"));
	editToolBar->addAction(deleteAction);
	editToolBar->addAction(clearAll);
	editToolBar->addAction(toFrontAction);
	editToolBar->addAction(sendBackAction);

	fontCombo = new QFontComboBox();
	connect(fontCombo, SIGNAL(currentFontChanged(QFont)),
		this, SLOT(currentFontChanged(QFont)));

	fontSizeCombo = new QComboBox;
	fontSizeCombo->setEditable(true);
	for (int i = 8; i < 30; i = i + 2)
		fontSizeCombo->addItem(QString().setNum(i));
	QIntValidator *validator = new QIntValidator(2, 64, this);
	fontSizeCombo->setValidator(validator);
	connect(fontSizeCombo, SIGNAL(currentIndexChanged(QString)),
		this, SLOT(fontSizeChanged(QString)));

	fontColorToolButton = new QToolButton;
	fontColorToolButton->setPopupMode(QToolButton::MenuButtonPopup);
	fontColorToolButton->setMenu(createColorMenu(SLOT(textColorChanged()),
		Qt::black));
	textAction = fontColorToolButton->menu()->defaultAction();
	fontColorToolButton->setIcon(createColorToolButtonIcon(
		".\\images\\textpointer.png", Qt::black));
	fontColorToolButton->setAutoFillBackground(true);
	connect(fontColorToolButton, SIGNAL(clicked()),
		this, SLOT(textButtonTriggered()));



	fillColorToolButton = new QToolButton;
	fillColorToolButton->setPopupMode(QToolButton::MenuButtonPopup);
	fillColorToolButton->setMenu(createColorMenu(SLOT(itemColorChanged()),
		Qt::white));
	fillAction = fillColorToolButton->menu()->defaultAction();
	fillColorToolButton->setIcon(createColorToolButtonIcon(
		".\\images\\floodfill.png", Qt::white));
	connect(fillColorToolButton, SIGNAL(clicked()),
		this, SLOT(fillButtonTriggered()));


	lineColorToolButton = new QToolButton;
	lineColorToolButton->setPopupMode(QToolButton::MenuButtonPopup);
	lineColorToolButton->setMenu(createColorMenu(SLOT(lineColorChanged()),
		Qt::black));
	lineAction = lineColorToolButton->menu()->defaultAction();
	lineColorToolButton->setIcon(createColorToolButtonIcon(
		".\\images\\linecolor.png", Qt::black));
	connect(lineColorToolButton, SIGNAL(clicked()),
		this, SLOT(lineButtonTriggered()));

	textToolBar = addToolBar(tr("Font"));
	textToolBar->addWidget(fontCombo);
	textToolBar->addWidget(fontSizeCombo);
	textToolBar->addAction(boldAction);
	textToolBar->addAction(italicAction);
	textToolBar->addAction(underlineAction);

	colorToolBar = addToolBar(tr("Color"));
	colorToolBar->addWidget(fontColorToolButton);
	colorToolBar->addWidget(fillColorToolButton);
	colorToolBar->addWidget(lineColorToolButton);

	packetAction = new QAction(QIcon(".\\images\\packet.png"),
		tr("Add packet"), this);
	packetAction->setStatusTip(tr("Add packet"));
	connect(packetAction, SIGNAL(triggered()),
		this, SLOT(setItemInsert()));

	noteAction = new QAction(QIcon(".\\images\\note.png"),
		tr("Add note"), this);
	noteAction->setStatusTip(tr("Add note"));
	connect(noteAction, SIGNAL(triggered()),
		this, SLOT(setTextInsert()));

	QToolButton *pointerButton = new QToolButton;
	pointerButton->setCheckable(true);
	pointerButton->setChecked(true);
	pointerButton->setIcon(QIcon(".\\images\\pointer.png"));
	QToolButton *linePointerButton = new QToolButton;
	linePointerButton->setCheckable(true);
	linePointerButton->setIcon(QIcon(".\\images\\linepointer.png"));


	pointerTypeGroup = new QButtonGroup(this);
	pointerTypeGroup->addButton(pointerButton, int(DiagramScene::MoveItem));
	pointerTypeGroup->addButton(linePointerButton,
		int(DiagramScene::InsertLine));
	connect(pointerTypeGroup, SIGNAL(buttonClicked(int)),
		this, SLOT(pointerGroupClicked(int)));

	sceneScaleCombo = new QComboBox;
	QStringList scales;
	scales << tr("50%") << tr("75%") << tr("100%") << tr("125%") << tr("150%");
	sceneScaleCombo->addItems(scales);
	sceneScaleCombo->setCurrentIndex(2);
	connect(sceneScaleCombo, SIGNAL(currentIndexChanged(QString)),
		this, SLOT(sceneScaleChanged(QString)));

	pointerToolbar = addToolBar(tr("Pointer type"));
	pointerToolbar->addAction(packetAction);
	pointerToolbar->addAction(noteAction);
	pointerToolbar->addWidget(pointerButton);
	pointerToolbar->addWidget(linePointerButton);
	pointerToolbar->addWidget(sceneScaleCombo);

}

//�����, ���� ��������, �� �������! 
//���������� ��� ��������� ������ createToolbars(),createToolBox()
QWidget *MainWindow::createBackgroundCellWidget(const QString &text,
	const QString &image)
{
	QToolButton *button = new QToolButton;
	button->setText(text);
	button->setIcon(QIcon(image));
	button->setIconSize(QSize(50, 50));
	button->setCheckable(true);
	backgroundButtonGroup->addButton(button);

	QGridLayout *layout = new QGridLayout;
	layout->addWidget(button, 0, 0, Qt::AlignHCenter);
	layout->addWidget(new QLabel(text), 1, 0, Qt::AlignCenter);

	QWidget *widget = new QWidget;
	widget->setLayout(layout);

	return widget;
}

//�����, ���� ��������, �� �������!
//���������� ��� ��������� ������ createToolbars(),createToolBox()
QWidget *MainWindow::createCellWidget(const QString &text,
	DiagramItem::DiagramType type)
{

	DiagramItem item(type, itemMenu);
	QIcon icon(item.image());

	QToolButton *button = new QToolButton;
	button->setIcon(icon);
	button->setIconSize(QSize(50, 50));
	button->setCheckable(true);
	buttonGroup->addButton(button, int(type));

	QGridLayout *layout = new QGridLayout;
	layout->addWidget(button, 0, 0, Qt::AlignHCenter);
	layout->addWidget(new QLabel(text), 1, 0, Qt::AlignCenter);

	QWidget *widget = new QWidget;
	widget->setLayout(layout);

	return widget;
}

//������� ��������� ���� ������ �����
QMenu *MainWindow::createColorMenu(const char *slot, QColor defaultColor)
{
	QList<QColor> colors;
	colors << Qt::black << Qt::white << Qt::red << Qt::blue << Qt::yellow;
	QStringList names;
	names << tr("black") << tr("white") << tr("red") << tr("blue")
		<< tr("yellow");

	QMenu *colorMenu = new QMenu(this);
	for (int i = 0; i < colors.count(); ++i) {
		QAction *action = new QAction(names.at(i), this);
		action->setData(colors.at(i));
		action->setIcon(createColorIcon(colors.at(i)));
		connect(action, SIGNAL(triggered()),
			this, slot);
		colorMenu->addAction(action);
		if (colors.at(i) == defaultColor) {
			colorMenu->setDefaultAction(action);
		}
	}
	return colorMenu;
}

//�������� ������, � ���� ������ �����
QIcon MainWindow::createColorToolButtonIcon(const QString &imageFile,
	QColor color)
{
	QPixmap pixmap(50, 80);
	pixmap.fill(Qt::transparent);
	QPainter painter(&pixmap);
	QPixmap image(imageFile);
	QRect target(0, 0, 50, 60);
	QRect source(0, 0, 42, 42);
	painter.fillRect(QRect(0, 60, 50, 80), color);
	painter.drawPixmap(target, image, source);

	return QIcon(pixmap);
}

//�������� ������, � ���� ������ �����
QIcon MainWindow::createColorIcon(QColor color)
{
	QPixmap pixmap(20, 20);
	QPainter painter(&pixmap);
	painter.setPen(Qt::NoPen);
	painter.fillRect(QRect(0, 0, 20, 20), color);

	return QIcon(pixmap);
}

//���� ������� ��������� ������� �� �����. ���� ��������� ������� �������� DiagramItem, 
//�� ��� ����� ���� ������� �������, ������� � ��� ���������; �� �� ����� ��� �� �� ����� ���� �������, 
//������� �� ��������� � ���������� �� ����� ������.
void MainWindow::deleteItem()
{
	foreach (QGraphicsItem *item, scene->selectedItems()) {
		if (item->type() == Arrow::Type) {
			scene->DiagramScene::removePacket(item);
			scene->removeItem(item);
			Arrow *arrow = qgraphicsitem_cast<Arrow *>(item);
			arrow->startItem()->removeArrow(arrow);
			arrow->endItem()->removeArrow(arrow);
			delete item;
		}
	}

	foreach (QGraphicsItem *item, scene->selectedItems()) {
		if (item->type() == DiagramItem::Type) {
			qgraphicsitem_cast<DiagramItem *>(item)->removeArrows();
		}
		scene->DiagramScene::removePacket(item);
		scene->removeItem(item);
		delete item;
		scene->editCountPacket(0);
	}
}

//������� ���������� � ����
void MainWindow::sl_saveSceneToFile()
{
	int type;
	fileName=settings.value("saveFileName").toString();//��������� ���� ������������ �����
	fileName = QFileDialog::getSaveFileName (this, "��������� Dgrafer",fileName, "XML (*.xml)");
	settings.setValue("saveFileName",fileName);//���������� ���� ������������ �����

	//��������� ���������� �����
	QFile file(fileName);
	if (!file.open(QIODevice::WriteOnly))
	{
		return;
	}
	QTextStream out(&file);
	out.setCodec(QTextCodec::codecForName("UTF-8"));//������������� ����� ��� ������, � ����������� � ��� ������
	out << scene->items().count() << "\n";
	out << "<xml>\n";
	for(int i=0;i<scene->items().count();i++)//������� ���� ���������
	{
		type=scene->items().value(i)->type();
		//��������� ��� ���������� � ������
		if(type==65551)
		{
			QGraphicsItem* item=scene->items().value(i);
			DiagramItem* dItem=(DiagramItem*)item;
			//���� �����������
			if(dItem->diagramType()==DiagramItem::Comment)
			{
				out << "<Comment>\n";
				QPointF position;
				position = item->mapToScene(position);
				out << "<X>" << QString::number((int)position.x()) << "</X>\n";
				out << "<Y>" << QString::number((int)position.y()) << "</Y>\n";
				out << "</Comment>\n\n";
			}
			//���� �����
			if(dItem->diagramType()==DiagramItem::Step)
			{
				QPointF position;
				position = item->mapToScene(position);
				QString c;
				if(dItem->getIsPlus())
				{
					c="+";
				}
				else
					c="-";
				out << "<Packet>\n";
				out << "<X>" << QString::number((int)position.x()) << "</X>\n";
				out << "<Y>" << QString::number((int)position.y()) << "</Y>\n";
				out << "<ID>" << dItem->returnId() << "</ID>\n";
				out << "<isParent>" << dItem->getIsParent() << "</isParent>\n";
				out << "<isChild>" << dItem->getIsChild() << "</isChild>\n";
				//���� ����� �������� ���������
				if(dItem->getIsParent() == true)
				{
					//��������� ������� ����� � ������� ��������
					QList <DiagramItem *> list = dItem->getListChild();
					//���� � �������� ���� �������
					if(list.count() == 1)
					{
						out << "<childs1>" << list.value(0)->returnId() << "</childs1>\n";
						out << "<childs2>" << "-1" << "</childs2>\n";
						out << "<childs3>" << "-1" << "</childs3>\n";
					}
					//���� � �������� ��� �������
					else if(list.count() == 2)
					{
						out << "<childs1>" << list.value(0)->returnId() << "</childs1>\n";
						out << "<childs2>" << list.value(1)->returnId() << "</childs2>\n";
						out << "<childs3>" << "-1" << "</childs3>\n";
					}
					//���� � �������� ��� �������
					else if(list.count() == 3)
					{
						out << "<childs1>" << list.value(0)->returnId() << "</childs1>\n";
						out << "<childs2>" << list.value(1)->returnId() << "</childs2>\n";
						out << "<childs3>" << list.value(2)->returnId() << "</childs3>\n";
					}	
				}
				//���� ����� ���
				else
				{
					out << "<childs1>" << "-1" << "</childs1>\n";
					out << "<childs2>" << "-1" << "</childs2>\n";
					out << "<childs3>" << "-1" << "</childs3>\n";
				}
				out << "<isPlus>" << c << "</isPlus>\n";
				out << "</Packet>\n\n";
			}

		}
		//��������� ����
		else if(type==65539)
		{
			QGraphicsItem* item=scene->items().value(i);
			DiagramTextItem* text=(DiagramTextItem*)item;
			QPointF position;
			position = item->mapToScene(position);
			if(!(text->toPlainText() == "+") && !(text->toPlainText() == "-"))
			{
				out << "<Text>\n";
				out << "<X>" << QString::number((int)position.x()) << "</X>\n";//���������� X-����������
				out << "<Y>" << QString::number((int)position.y()) << "</Y>\n";//���������� Y-����������
				out << "<Txt>" << text->toPlainText() << "</Txt>\n";//���������� ������
				out << "<TxtW>" << text->textWidth() << "</TxtW>\n";//���������� ������ ������
				//out << "<Text>" << text->defaultTextColor() << "<\Text>\n";//���������� ����� ������
				out << "<TxtS>" << text->font().underline() << "</TxtS>\n";//���������� ������������� ������
				out << "<TxtC>" << text->font().styleHint() << "</TxtC>\n";//���������� ������� ������
				out << "</Text>\n\n";
			}
		}
		//��������� �����
		else if(type==65540)
		{
			QGraphicsItem* item=scene->items().value(i);
			Arrow* arrow=(Arrow*)item;
			DiagramItem *startItem = arrow->myStartItem;
			DiagramItem *EndItem = arrow->myEndItem;
			QPointF positionStartItem;
			QPointF positionEndItem;
			positionStartItem.setX(0);
			positionStartItem.setY(0);
			positionEndItem.setX(0);
			positionEndItem.setY(0);
			positionStartItem = startItem->mapToScene(positionStartItem);
			positionEndItem = EndItem->mapToScene(positionEndItem);
			out << "<Connect>\n";
			//���������� ������� �������-���������
			out << "<X1>" << (int)positionStartItem.x() << "</X1>\n";
			out << "<Y1>"<< (int)positionStartItem.y() <<"</Y1>\n";
			//���������� ������� �������-����
			out << "<X2>" << (int)positionEndItem.x() << "</X2>\n";
			out << "<Y2>" << (int)positionEndItem.y() << "</Y2>\n";
			out << "</Connect>\n\n";
			positionStartItem.setX(0);
			positionStartItem.setY(0);
			positionEndItem.setX(0);
			positionEndItem.setY(0);
		}
	}
	out << "</xml>";
	QMessageBox::warning(this,"�������","���������� ����� ������ �������");
	file.close();//��������� ����
}



//������� �������� �� �����
void MainWindow::sl_loadSceneFromFile()
{
	QFileDialog::Options options;
	options = 0;
	QString selectedFilter;
	int count;
	QString str;

	fileNameLoad=settings.value("saveFileNameLoad").toString();//��������� ���� ������������ �����
	fileNameLoad = QFileDialog::getOpenFileName(this,
		tr("��������� ���������"),
		fileNameLoad,
		tr("XML files (*.xml)"),
		&selectedFilter,
		options);
	settings.setValue("saveFileNameLoad",fileNameLoad);//���������� ���� ������������ �����
	QFile file(fileNameLoad);
	if (!file.open(QIODevice::ReadOnly))
	{
		QMessageBox::warning(this,"Warning","Cant open file");
		return;
	}
	QTextStream in(&file);
	in.setCodec(QTextCodec::codecForName("UTF-8"));
	in >> count;
	str = in.readAll();
	scene->clear();
	loadElements(str, count);
	file.close();
}

void MainWindow::loadone(){
	loadMasElement(1);
}

void MainWindow::loadtwo(){
	loadMasElement(2);}

void MainWindow::loadthree(){
	loadMasElement(3);
}

//������� ��������� �������� ���������
void MainWindow::loadMasElement(int a)
{
	QString  fileName;
	int count;
	if(a == 1){
		fileName = ".\\1.xml";
	}
	else if(a == 2){
		fileName = ".\\2.xml";
	}
	else if(a == 3){
		fileName = ".\\3.xml";
	}

	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly)){
		return;}

	QTextStream in(&file);
	in.setCodec(QTextCodec::codecForName("UTF-8"));
	in >> count;
	QString str = in.readAll();

	//����� ��� ��������
	loadElements(str, count);
	//����� ����
	file.close();
}

//������� ������ ��������� �� ����� ��� ���������� 
//������� �������� � �������� ��������
void MainWindow::loadElements(QString str, int count)
{
	QString ref; //���������� ��� ���������� ������
	int x=0, y=0, w=0, c=0, s=0, x1=0, y1=0, x2=0, y2=0; //���������� ��� �������� ���������� ������
	int mas[100][4]; //������ �������� ��� ����������� �����
	int k=0;//���������� ����� �������� ���������
	int z = 0;//���������� ����� �������� ���������
	QString type;//������ ���� ��������
	int type1, type2;//��� ���� ��������
	QString isParent, isChild, sChild, refIsPlus;//������ ������������ ��������� //��������//�������//������ ����� ��������
	int id;//ID ������������ ������
	int parent, child, childs1, childs2, childs3, isPlus;//������ ������������ ��������� //��������//�������//������ ����� ��������
	int packet[100][9];//����� ����������� �������
	QXmlStreamReader xml(str);//��������� ��� ���� XML

	//���� �� ��������� ����� ������ xml ���������� ��������
	while (!xml.atEnd() && !xml.hasError())
	{
		//��������� ������ �����(���)
		QXmlStreamReader::TokenType token = xml.readNext();
		if (xml.name() == "xml")//���� ������ ��� ����� ��� XML, �������� � ��������� �������� �����
			continue;
		//���� ����������� ��� ����������, ���������� ������, ������� ��������� �� ������ 
		if(token == QXmlStreamReader::Invalid) 
		{
			QString error = xml.errorString();
			scene->clear();
			QMessageBox::warning(this,"Warning","���������� ���������� ��� � ������");
			return;
		}
		//���� ����������� ��� �������� ����������� �����
		if (token == QXmlStreamReader::StartElement)
		{
			//���� �� �������� ���������
			if(xml.name() == "xml")
				continue;//��������� � ��������� �������� ����� 
			//���� ��� ����������
			if (xml.name() == "Comment")
			{
				xml.readNext();//������ ��������� �������
				//���� �� ������ �� ������������ ���� ���������� ���������...
				while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "Comment"))
				{
					token = xml.tokenType();
					//������� ����� ������������� �����
					if(token == QXmlStreamReader::Invalid) 
					{
						scene->clear();
						QMessageBox::warning(this,"Warning","������� ����� ������������� �����");
						return;
					}
					//���� �� �������� ��������� ������� ��� ������ ����������
					if (xml.tokenType() == QXmlStreamReader::StartElement)
					{
						//���� �������� ���������� �
						if(xml.name() == "X")
						{
							//���������� ��������� 
							QString refx = xml.readElementText();
							x = refx.toInt();
							//xml.readNext();
							if(x > 5000 || x < 0)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","�������� ����� �������� ���������� � �����������");
								return;
							}
						}
						//���� �������� ���������� Y
						else if(xml.name() == "Y")
						{
							//���������� ��������� 
							QString refy = xml.readElementText();
							y = refy.toInt();
							//xml.readNext();
							if(y > 5000 || y < 0)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","�������� ����� �������� ���������� Y �����������");
								return;
							}
						}
						//�����
						else
						{
							scene->clear();
							QMessageBox::warning(this,"Warning","������� ����� ����. ���������� ������������ ��� � ���� ����������");
							return;
						}
					}
					//��������� � ���������� ��������
					xml.readNextStartElement();
				}
				//�������� ������� �������� ����������
				scene->setItemType(DiagramItem::DiagramType(1));
				scene->DiagramScene::insertClass(x, y);	
				x=0; y=0;
			}
			//���� ��� ������
			else if(xml.name() == "Packet")
			{
				//��������� ��������� �������
				xml.readNext();
				//���� �� ������ �� ������������ ���� ���������� ���������...
				while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "Packet"))
				{
					token = xml.tokenType();
					//������� ����� ������������� �����
					if(token == QXmlStreamReader::Invalid) 
					{
						scene->clear();
						QMessageBox::warning(this,"Warning","Crushed file");
						return;
					}
					//���� �� �������� ��������� ������� ��� ������ ������
					if (xml.tokenType() == QXmlStreamReader::StartElement)
					{
						//���� �������� ���������� �
						if(xml.name() == "X")
						{
							//���������� ���������
							QString refx = xml.readElementText();
							x = refx.toInt();
							if(x > 5000 || x < 0)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","�������� ����� �������� ���������� � ������");
								return;
							}
						}
						//���� �������� ���������� Y
						else if(xml.name() == "Y")
						{
							//���������� ��������� 
							QString refy = xml.readElementText();
							y = refy.toInt();
							if(y > 5000 || y < 0)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","�������� ����� �������� ���������� Y ������");
								return;
							}
						}
						//���� �������� ID �������
						else if(xml.name() == "ID")
						{
							//���������� ��������� 
							QString ref = xml.readElementText();
							id = ref.toInt();
							if(id > 100 || id < 0)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","�������� ����� �������� ID ������");
								return;
							}
						}
						//���� �������� �������� ��������
						else if(xml.name() == "isParent")
						{
							//���������� ���������� 
							isParent = xml.readElementText();
							parent = isParent.toInt();
							if(parent>1 && parent<0)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","���������� ����� �������� ���������� ��������");
								return;
							}
						}
						//���� �������� �������� �������
						else if(xml.name() == "isChild")
						{
							//���������� ����������
							isChild = xml.readElementText();
							child = isChild.toInt();
							if(child>1 && child<0)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","���������� ����� �������� ���������� �������");
								return;
							}
						}
						//���� �������� �������� ������ �������
						else if(xml.name() == "childs1")
						{
							//���������� ����������
							sChild = xml.readElementText();
							childs1 = sChild.toInt();
							if(childs1>100 && childs1<-1)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","���������� ������ ������� ������� �����");
								return;
							}
						}
						//���� �������� �������� ������� �������
						else if(xml.name() == "childs2")
						{
							//���������� ����������
							sChild = xml.readElementText();
							childs2 = sChild.toInt();
							if(childs2>100 && childs2<-1)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","���������� ������ ������� ������� �����");
								return;
							}
						}
						//���� �������� �������� �������� �������
						else if(xml.name() == "childs3")
						{
							//���������� ����������
							sChild = xml.readElementText();
							childs3 = sChild.toInt();
							if(childs3>100 && childs3<-1)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","���������� ������ �������� ������� �����");
								return;
							}
						}
						else if(xml.name() == "isPlus")
						{
							//���������� ����������
							refIsPlus = xml.readElementText();
							if(refIsPlus!="+" && refIsPlus!="-")
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","���������� ���� ���������� �����");
								return;
							}
						}
						//�����
						else
						{
							scene->clear();
							QMessageBox::warning(this,"Warning","������� ����� ����. ���������� ������������ ��� � ���� �����");
							return;
						}
					}				
					xml.readNextStartElement();//��������� � ���������� ���������� �������� � ������ ���
				}
				//���������� ��� ������ � �����
				//0 - ���������� X
				//1 - ���������� Y
				//2 - bool parent
				//3 - bool child
				//4 - ID childs1
				//5 - ID childs1
				//6 - ID childs1
				//7 - ID ������
				packet[z][0] = x;
				packet[z][1] = y;
				packet[z][2] = parent;
				packet[z][3] = child;
				packet[z][4] = childs1;
				packet[z][5] = childs2;
				packet[z][6] = childs3;
				packet[z][7] = id;
				if(refIsPlus == "+")
				{
					packet[z][8] = 1;
				}
				else
				{
					packet[z][8] = 0;
				}
				z++;
			}
			//���� ��� ������
			else if(xml.name() == "Text")
			{
				xml.readNext();//������ ��������� �������
				//���� �� ������ �� ������������ ���� ���������� ���������...
				while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "Text"))
				{
					token = xml.tokenType();
					//������� ����� ������������� �����
					if(token == QXmlStreamReader::Invalid) 
					{
						scene->clear();
						QMessageBox::warning(this,"Warning","Crushed file");
						return;
					}
					//���� �� �������� ��������� ������� ��� ������ �����
					if (xml.tokenType() == QXmlStreamReader::StartElement)
					{
						//���� �������� ���������� �
						if(xml.name() == "X")
						{
							//���������� ��������� 
							QString refx = xml.readElementText();
							x = refx.toInt();
							if(x > 5000 || x < 0)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","�������� ����� �������� ���������� � ������");
								return;
							}
						}
						//���� �������� ���������� Y
						else if(xml.name() == "Y")
						{
							//���������� ��������� 
							QString refy = xml.readElementText();
							y = refy.toInt();
							if(y > 5000 || y < 0)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","�������� ����� �������� ���������� Y ������");
								return;
							}
						}
						//���� ������� ��� �����
						else if(xml.name() == "Txt")
						{
							ref = xml.readElementText();
						}
						//������� ������ ������
						else if(xml.name() == "TxtW")
						{
							QString refw = xml.readElementText();
							w = refw.toInt();
						}
						//
						else if(xml.name() == "TxtS")
						{
							QString refs = xml.readElementText();
							s = refs.toInt();
						}
						else if(xml.name() == "TxtC")
						{
							QString refc = xml.readElementText();
							c = refc.toInt();
						}
						//�����
						else
						{
							scene->clear();
							QMessageBox::warning(this,"Warning","������� ����� ����. ���������� ������������ ��� � ���� ������");
							return;
						}
					}
					xml.readNextStartElement();
				}
				scene->DiagramScene::insertText(x, y, ref, w, s, c);

			}
			//���� ��� �����
			else if(xml.name() == "Connect")
			{
				xml.readNext();//������ ��������� �������
				//���� �� ������ �� ������������ ���� ����� ���������...
				while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "Connect"))
				{
					token = xml.tokenType();
					//������� ����� ������������� �����
					if(token == QXmlStreamReader::Invalid) 
					{
						scene->clear();
						QMessageBox::warning(this,"Warning","������� ����� ������������� �����");
						return;
					}
					//���� �� �������� ��������� ������� ��� ������ �����
					if (xml.tokenType() == QXmlStreamReader::StartElement)
					{
						//���� �������� ���������� � ���������� ������ 
						if(xml.name() == "X1")
						{
							//���������� ��������� 
							QString refx1 = xml.readElementText();
							x1 = refx1.toInt();
							if(x1 > 5000 || x1 < 0)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","�������� ����� �������� ���������� � ���������� ������");
								return;
							}
						}
						else if(xml.name() == "Y1")
						{
							//���������� ��������� 
							QString refy1 = xml.readElementText();
							y1 = refy1.toInt();
							if(y1 > 5000 || y1 < 0)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","�������� ����� �������� ���������� Y ���������� ������");
								return;
							}
						}
						else if(xml.name() == "X2")
						{
							//���������� ��������� 
							QString refx2 = xml.readElementText();
							x2 = refx2.toInt();
							if(x2 > 5000 || x2 < 0)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","�������� ����� �������� ���������� � ��������� ������");
								return;
							}
						}
						else if(xml.name() == "Y2")
						{
							//���������� ��������� 
							QString refy2 = xml.readElementText();
							y2 = refy2.toInt();
							if(y2 > 5000 || y2 < 0)
							{
								scene->clear();
								QMessageBox::warning(this,"Warning","�������� ����� �������� ���������� � ��������� ������");
								return;
							}
						}
						//�����
						else
						{
							scene->clear();
							QMessageBox::warning(this,"Warning","������� ����� ����. ���������� ������������ ��� � ���� �����");
							return;
						}
					}
					xml.readNextStartElement();//��������� � ���������� ���������� ��������
				}
				//���������� ������ � ������
				mas[k][0] = x1;
				mas[k][1] = y1;
				mas[k][2] = x2;
				mas[k][3] = y2;
				k++;		
			}//�����
			else
			{
				QMessageBox::warning(this,"Warning","������� ����� ����. ���������� ������������  ��������� ���");
				return;
			}
		}
	}

	//���� �������� ���� �������
	for(int l=0; l<=z; l++)
	{
		//���� ����� �� �������� ��������
		if(!packet[l][3] == 1)
		{
			scene->setItemType(DiagramItem::DiagramType(0));
			scene->DiagramScene::insertPacket(packet[l][0], packet[l][1], packet[l][7], packet[l][8]);			
		}
	}
	//���� �������� ���� ������� �����
	for(int l=0; l<=z; l++)
	{
		if(packet[l][2] == 1)
		{
			for(int i=0; i<=z; i++)
			{
				if(packet[i][3] == 1)
				{
					if(packet[i][7] == packet[l][4])
					{
						scene->setItemType(DiagramItem::DiagramType(0));
						scene->DiagramScene::insertPacket(packet[i][0], packet[i][1], (bool)packet[i][3], packet[i][7], packet[l][7], packet[i][8]);
					}
					else if(packet[i][7] == packet[l][5])
					{
						scene->setItemType(DiagramItem::DiagramType(0));
						scene->DiagramScene::insertPacket(packet[i][0], packet[i][1], (bool)packet[i][3], packet[i][7], packet[l][7], packet[i][8]);
					}
					else if(packet[i][7] == packet[l][6])
					{
						scene->setItemType(DiagramItem::DiagramType(0));
						scene->DiagramScene::insertPacket(packet[i][0], packet[i][1], (bool)packet[i][3], packet[i][7], packet[l][7], packet[i][8]);
					}
				}
			}
		}
	}

	//���� ��� ������������� ���� ��������� ��������� � ������� � �����������
	int n = scene->items().count();
	QList <QGraphicsItem *> list = scene->items();
	for(int i=0;i<n;i++)//������� ���� ���������
	{
		type1=list.value(i)->type();
		if(type1 == 65539)//���� ������� �������� �������
		{
			QGraphicsItem* item1=list.value(i);
			DiagramTextItem* text=(DiagramTextItem*)item1;
			QString str = text->toPlainText();
			QPointF position1;
			position1 = item1->mapToScene(position1);
			for(int j=0; j<list.count(); j++)//������� ���� ���������
			{
				type2=list.value(j)->type();
				if(type2 == 65551)//���� ������� �������� ������� ��� �����������
				{
					QGraphicsItem* item2=list.value(j);
					QPointF position2;
					position2 = item2->mapToScene(position2);
					if(position1.x() == (position2.x() - 60) && position1.y() == (position2.y() - 75))//������� ��� ������� �������� ������
					{
						text->setParentItem(item2);
						text->setPos(-60, -75);
						text->setPlainText(text->toPlainText());
					}
					if(position1.x() == (position2.x() - 70) && position1.y() == (position2.y() - 50))//������� ��� ������� �������� ������
					{
						text->setParentItem(item2);
						text->setPos(-70, -50);
						text->setPlainText(text->toPlainText());
					}
					if(position1.x() == (position2.x() - 60) && position1.y() == (position2.y() - 40))//������� ��� ������� ����������
					{
						text->setParentItem(item2);
						text->setPos(-60, -40);
						text->setPlainText(text->toPlainText());
					}
				}
			}
		}

	}

	//���� ��������������� ������
	for(int g=0; g<k; g++)
	{
		QLineF lineF;
		QGraphicsItem * item1=scene->itemAt(mas[g][0], mas[g][1]);
		if(item1->type() == 65540)
		{
			mas[g][0] +=10;
			mas[g][1] +=10;
		}
		item1=scene->itemAt(mas[g][0], mas[g][1]);
		if(item1->type() == 65540)
		{
			mas[g][0] -=20;
			mas[g][1] -=20;
			item1=scene->itemAt(mas[g][0], mas[g][1]);

		}
		DiagramItem *startItem = qgraphicsitem_cast<DiagramItem *>(item1);
		QGraphicsItem * item2=scene->itemAt(mas[g][2],mas[g][3]);
		DiagramItem *endItem = qgraphicsitem_cast<DiagramItem *>(item2);
		Arrow *arrow = new Arrow(startItem, endItem);
		startItem->addArrow(arrow);
		endItem->addArrow(arrow);
		arrow->setZValue(-1.0);
		scene->addItem(arrow);
		arrow->updatePosition();	
	}
	QMessageBox::warning(this,"�������","�������� ����� ������ �������");
}
//������� ���������� ������� ������� � JPG
void MainWindow::export_picture()
{
	QFileDialog::Options options;
	options = 0;
	QString selectedFilter;
	fileNamePic=settings.value("saveFileNamePic").toString();
	fileNamePic = QFileDialog::getSaveFileName(this,
		tr("���������� � �������� ��� � PDF"),
		fileNamePic,
		tr("Png (*.png);;Pdf (*.pdf)"),
		&selectedFilter,
		options);
	settings.setValue("saveFileNamePic",fileNamePic);
	if (!fileNamePic.isEmpty()){
		if((selectedFilter == "Pdf (*.pdf)")) {
			QRectF rect = scene->itemsBoundingRect();

			QPrinter printer;
			printer.setOutputFileName(fileNamePic);

			QSizeF size = printer.paperSize(QPrinter::Millimeter);
			size.setHeight(size.width()*rect.height()/rect.width());
			printer.setPaperSize(size,QPrinter::Millimeter);
			printer.setPageMargins(0,0,0,0,QPrinter::Millimeter);

			QPainter painter(&printer);// generate PDF
			painter.setRenderHint(QPainter::Antialiasing);
			scene->render(&painter, QRectF(), rect);
		}
		else {
			QPixmap pixmap(1000,1000);
			pixmap.fill();

			QPainter painter(&pixmap);
			painter.setRenderHint(QPainter::Antialiasing);

			QRectF rect=scene->itemsBoundingRect();
			scene->render(&painter, QRectF(), rect);
			painter.end();

			pixmap.save(fileNamePic);
		}
	}
}

////���� ���������� �� DiagramScene ����� ������� ����������� �� �����. �� ������������� ����� ����� ������� �� �����
void MainWindow::itemInserted(DiagramItem *item)
{ 
	scene->setMode(DiagramScene::Mode(3));
}

//���� ���������� �� DiagramScene ����� ������� ����������� �� �����. �� ������������� ����� ����� ������� �� �����
void MainWindow::textInserted(QGraphicsTextItem *)
{
	scene->setMode(DiagramScene::Mode(3));
}

//���� ���������� ����� ���������� ������� � DiagramScene. � ������ ������� ������ ������ �������������� ������ ���������� ����������, 
//������� ��� �� ���� ��������� ����� ����� �������� �� ��������.
void MainWindow::itemSelected(QGraphicsItem *item)
{
	DiagramTextItem *textItem = qgraphicsitem_cast<DiagramTextItem *>(item);
}

//���� ������������� ��� ��������� ������
void MainWindow::item_Selected()
{	
	DiagramScene::Mode mode;	//���������� ��� ������ �������������� ���������
	mode = scene->returnMode();	//����������� ������ �������������� ���������

	//���� �� ������ ������ �� ���� �������� ������ ����� ��������
	if(mode != DiagramScene::InsertLine)
		scene->setMode(DiagramScene::MoveItem);	//������������ ������ �������������� ��������� � ��������� "��������� �������"
}

//���� ���������� �� ������� ������ �� �����
void MainWindow::setItemInsert()
{
	scene->setItemType(DiagramItem::DiagramType(0));
	scene->setMode(DiagramScene::InsertItem);
}

//���� ���������� �� ������� ������ �� �����
void MainWindow::setTextInsert()
{
	scene->setItemType(DiagramItem::DiagramType(1));	//������� ������
	scene->setMode(DiagramScene::InsertItem);	//������������ ������ �������������� ��������� � ��������� "������� ������"
	//scene->setMode(DiagramScene::InsertText);	//������������ ������ �������������� ��������� � ��������� "������� ������"
}

//���� ���������� �� ������� ����� �� �����
void MainWindow::setLineInsert()
{	
	scene->setMode(DiagramScene::InsertLine);
}
//������� ������� �������� ����
void MainWindow::clearItem()
{ 	
	QMessageBox msgBox;
	msgBox.setText("�� ������������� ������ �������� ����� ��� ����������?");
	msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Save);
	int ret = msgBox.exec();
	switch (ret) {
	case QMessageBox::Save:
		// Save was clicked
		sl_saveSceneToFile();
		scene->clear();
		break;
	case QMessageBox::Discard:
		// Don't Save was clicked
		scene->clear();
		break;
	case QMessageBox::Cancel:
		// Cancel was clicked
		break;
	default:
		// should never be reached
		break;
	}

}

//������� ������
void MainWindow::print()
{
	QPrinter p;

	QPrintDialog qp (&p, this);
	if (qp.exec() == QDialog::Accepted)
	{
		qDebug () << "printerDialog";
		int a, b = 0;
		a = QMainWindow::x();
		b = QMainWindow::y();
		a = a*1.5;
		b = b*2;
		QRect rect (0, 0, a, b);
		QPixmap qpm = QPixmap::grabWidget(view, rect);

		QPainter painter;
		qpm = qpm.scaled(p.pageRect().width(), p.pageRect().height(), Qt::KeepAspectRatio);
		painter.begin (&p);
		painter.drawPixmap (0, 0, qpm);
		painter.end();
	}
}


//���� "������"
void MainWindow::Help()
{
	HINSTANCE hIns=ShellExecute(0, L"open",L"Help.pdf", L"", L"", SW_SHOW);
	HINSTANCE hIns1=ShellExecute(0, L"open",L"Help.txt", L"", L"", SW_SHOW);
}
//���� "� ���������"
void MainWindow::aboutProj()
{
	QMessageBox::about(this, tr("� ���������"),
		tr("����������� �������� �������� �������\n������ 1.0.\n\n��������� ����������� �������� ������ ���-463:\n\n������� ����\n������� �����\n������� �������\n\n\n\nALL RIGHTS RESERVED, 2013:)"));
}
void MainWindow::backgroundButtonGroupClicked(QAbstractButton *button)
{
	QList<QAbstractButton *> buttons = backgroundButtonGroup->buttons();
	foreach (QAbstractButton *myButton, buttons) {
		if (myButton != button)
			myButton->setChecked(false);
	}
	QString text = button->text();
	if (text == tr("Blue Grid"))
		scene->setBackgroundBrush(QPixmap(":/images/background1.png"));
	else if (text == tr("White Grid"))
		scene->setBackgroundBrush(QPixmap(":/images/background2.png"));
	else if (text == tr("Gray Grid"))
		scene->setBackgroundBrush(QPixmap(":/images/background3.png"));
	else
		scene->setBackgroundBrush(QPixmap(":/images/background4.png"));

	scene->update();
	view->update();
}

void MainWindow::buttonGroupClicked(int id)
{
	QList<QAbstractButton *> buttons = buttonGroup->buttons();
	foreach (QAbstractButton *button, buttons) {
		if (buttonGroup->button(id) != button)
			button->setChecked(false);
	}
	if (id == InsertTextButton) {
		scene->setMode(DiagramScene::InsertText);
	} else {
		scene->setItemType(DiagramItem::DiagramType(id));
		scene->setMode(DiagramScene::InsertItem);
	}
}

void MainWindow::pointerGroupClicked(int)
{
	scene->setMode(DiagramScene::Mode(pointerTypeGroup->checkedId()));
}