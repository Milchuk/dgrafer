/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      37,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      28,   11,   11,   11, 0x08,
      44,   11,   11,   11, 0x08,
      60,   11,   11,   11, 0x08,
      78,   73,   11,   11, 0x08,
     105,   73,   11,   11, 0x08,
     138,   73,   11,   11, 0x08,
     167,   11,   11,   11, 0x08,
     183,   11,   11,   11, 0x08,
     198,   11,   11,   11, 0x08,
     216,  211,   11,   11, 0x08,
     247,  242,   11,   11, 0x08,
     278,  272,   11,   11, 0x08,
     305,   11,   11,   11, 0x08,
     327,   11,   11,   11, 0x08,
     349,   11,   11,   11, 0x08,
     371,   11,   11,   11, 0x08,
     390,   11,   11,   11, 0x08,
     409,   11,   11,   11, 0x08,
     428,   11,   11,   11, 0x08,
     454,  447,   11,   11, 0x08,
     504,  501,   11,   11, 0x08,
     528,  501,   11,   11, 0x08,
     553,   11,   11,   11, 0x08,
     574,   11,   11,   11, 0x08,
     599,  597,   11,   11, 0x08,
     629,  619,   11,   11, 0x08,
     655,   11,   11,   11, 0x08,
     665,   11,   11,   11, 0x08,
     675,   11,   11,   11, 0x08,
     687,   11,   11,   11, 0x08,
     704,   11,   11,   11, 0x08,
     716,   11,   11,   11, 0x08,
     724,   11,   11,   11, 0x08,
     736,   11,   11,   11, 0x08,
     743,   11,   11,   11, 0x08,
     758,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0setTextInsert()\0setLineInsert()\0"
    "setItemInsert()\0deleteItem()\0item\0"
    "itemInserted(DiagramItem*)\0"
    "textInserted(QGraphicsTextItem*)\0"
    "itemSelected(QGraphicsItem*)\0"
    "item_Selected()\0bringToFront()\0"
    "sendToBack()\0font\0currentFontChanged(QFont)\0"
    "size\0fontSizeChanged(QString)\0scale\0"
    "sceneScaleChanged(QString)\0"
    "textButtonTriggered()\0fillButtonTriggered()\0"
    "lineButtonTriggered()\0textColorChanged()\0"
    "itemColorChanged()\0lineColorChanged()\0"
    "handleFontChange()\0button\0"
    "backgroundButtonGroupClicked(QAbstractButton*)\0"
    "id\0buttonGroupClicked(int)\0"
    "pointerGroupClicked(int)\0sl_saveSceneToFile()\0"
    "sl_loadSceneFromFile()\0a\0loadMasElement(int)\0"
    "str,count\0loadElements(QString,int)\0"
    "loadone()\0loadtwo()\0loadthree()\0"
    "export_picture()\0clearItem()\0print()\0"
    "aboutProj()\0Help()\0readSettings()\0"
    "writeSettings()\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->setTextInsert(); break;
        case 1: _t->setLineInsert(); break;
        case 2: _t->setItemInsert(); break;
        case 3: _t->deleteItem(); break;
        case 4: _t->itemInserted((*reinterpret_cast< DiagramItem*(*)>(_a[1]))); break;
        case 5: _t->textInserted((*reinterpret_cast< QGraphicsTextItem*(*)>(_a[1]))); break;
        case 6: _t->itemSelected((*reinterpret_cast< QGraphicsItem*(*)>(_a[1]))); break;
        case 7: _t->item_Selected(); break;
        case 8: _t->bringToFront(); break;
        case 9: _t->sendToBack(); break;
        case 10: _t->currentFontChanged((*reinterpret_cast< const QFont(*)>(_a[1]))); break;
        case 11: _t->fontSizeChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: _t->sceneScaleChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 13: _t->textButtonTriggered(); break;
        case 14: _t->fillButtonTriggered(); break;
        case 15: _t->lineButtonTriggered(); break;
        case 16: _t->textColorChanged(); break;
        case 17: _t->itemColorChanged(); break;
        case 18: _t->lineColorChanged(); break;
        case 19: _t->handleFontChange(); break;
        case 20: _t->backgroundButtonGroupClicked((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        case 21: _t->buttonGroupClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->pointerGroupClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->sl_saveSceneToFile(); break;
        case 24: _t->sl_loadSceneFromFile(); break;
        case 25: _t->loadMasElement((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 26: _t->loadElements((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 27: _t->loadone(); break;
        case 28: _t->loadtwo(); break;
        case 29: _t->loadthree(); break;
        case 30: _t->export_picture(); break;
        case 31: _t->clearItem(); break;
        case 32: _t->print(); break;
        case 33: _t->aboutProj(); break;
        case 34: _t->Help(); break;
        case 35: _t->readSettings(); break;
        case 36: _t->writeSettings(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 37)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 37;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
