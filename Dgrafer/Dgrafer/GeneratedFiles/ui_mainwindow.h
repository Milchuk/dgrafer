/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindowClass
{
public:
    QAction *actionSetItem;
    QAction *actionSetLine;
    QAction *actionSetText;
    QAction *actionDelite;
    QAction *actionSave;
    QAction *actionLoad;
    QAction *actionExport;
    QAction *undo;
    QAction *rendo;
    QAction *print;
    QAction *actionExport2;
    QAction *action_2;
    QAction *action_3;
    QAction *action1;
    QAction *action2;
    QAction *action3;
    QAction *clearScene;
    QWidget *centralWidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *layout;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnAddPackege;
    QPushButton *btnAddconnect;
    QPushButton *btnAddComment;
    QPushButton *btnDelite;
    QMenuBar *menuBar;
    QMenu *menu;
    QMenu *menu_4;
    QMenu *menu_2;
    QMenu *menu_3;

    void setupUi(QMainWindow *MainWindowClass)
    {
        if (MainWindowClass->objectName().isEmpty())
            MainWindowClass->setObjectName(QString::fromUtf8("MainWindowClass"));
        MainWindowClass->resize(780, 452);
        MainWindowClass->setMinimumSize(QSize(780, 450));
        actionSetItem = new QAction(MainWindowClass);
        actionSetItem->setObjectName(QString::fromUtf8("actionSetItem"));
        actionSetLine = new QAction(MainWindowClass);
        actionSetLine->setObjectName(QString::fromUtf8("actionSetLine"));
        actionSetText = new QAction(MainWindowClass);
        actionSetText->setObjectName(QString::fromUtf8("actionSetText"));
        actionDelite = new QAction(MainWindowClass);
        actionDelite->setObjectName(QString::fromUtf8("actionDelite"));
        actionSave = new QAction(MainWindowClass);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        actionLoad = new QAction(MainWindowClass);
        actionLoad->setObjectName(QString::fromUtf8("actionLoad"));
        actionExport = new QAction(MainWindowClass);
        actionExport->setObjectName(QString::fromUtf8("actionExport"));
        undo = new QAction(MainWindowClass);
        undo->setObjectName(QString::fromUtf8("undo"));
        rendo = new QAction(MainWindowClass);
        rendo->setObjectName(QString::fromUtf8("rendo"));
        print = new QAction(MainWindowClass);
        print->setObjectName(QString::fromUtf8("print"));
        actionExport2 = new QAction(MainWindowClass);
        actionExport2->setObjectName(QString::fromUtf8("actionExport2"));
        action_2 = new QAction(MainWindowClass);
        action_2->setObjectName(QString::fromUtf8("action_2"));
        action_3 = new QAction(MainWindowClass);
        action_3->setObjectName(QString::fromUtf8("action_3"));
        action1 = new QAction(MainWindowClass);
        action1->setObjectName(QString::fromUtf8("action1"));
        action2 = new QAction(MainWindowClass);
        action2->setObjectName(QString::fromUtf8("action2"));
        action3 = new QAction(MainWindowClass);
        action3->setObjectName(QString::fromUtf8("action3"));
        clearScene = new QAction(MainWindowClass);
        clearScene->setObjectName(QString::fromUtf8("clearScene"));
        centralWidget = new QWidget(MainWindowClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 30, 641, 391));
        layout = new QVBoxLayout(verticalLayoutWidget);
        layout->setSpacing(6);
        layout->setContentsMargins(11, 11, 11, 11);
        layout->setObjectName(QString::fromUtf8("layout"));
        layout->setContentsMargins(0, 0, 0, 0);
        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(30, 0, 581, 25));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        btnAddPackege = new QPushButton(horizontalLayoutWidget);
        btnAddPackege->setObjectName(QString::fromUtf8("btnAddPackege"));

        horizontalLayout->addWidget(btnAddPackege);

        btnAddconnect = new QPushButton(horizontalLayoutWidget);
        btnAddconnect->setObjectName(QString::fromUtf8("btnAddconnect"));

        horizontalLayout->addWidget(btnAddconnect);

        btnAddComment = new QPushButton(horizontalLayoutWidget);
        btnAddComment->setObjectName(QString::fromUtf8("btnAddComment"));

        horizontalLayout->addWidget(btnAddComment);

        btnDelite = new QPushButton(horizontalLayoutWidget);
        btnDelite->setObjectName(QString::fromUtf8("btnDelite"));
        btnDelite->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(btnDelite);

        MainWindowClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindowClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 780, 21));
        menu = new QMenu(menuBar);
        menu->setObjectName(QString::fromUtf8("menu"));
        menu_4 = new QMenu(menu);
        menu_4->setObjectName(QString::fromUtf8("menu_4"));
        menu_2 = new QMenu(menuBar);
        menu_2->setObjectName(QString::fromUtf8("menu_2"));
        menu_3 = new QMenu(menuBar);
        menu_3->setObjectName(QString::fromUtf8("menu_3"));
        MainWindowClass->setMenuBar(menuBar);

        menuBar->addAction(menu_3->menuAction());
        menuBar->addAction(menu->menuAction());
        menuBar->addAction(menu_2->menuAction());
        menu->addAction(actionSetItem);
        menu->addAction(actionSetLine);
        menu->addAction(actionSetText);
        menu->addAction(menu_4->menuAction());
        menu_4->addAction(action1);
        menu_4->addAction(action2);
        menu_4->addAction(action3);
        menu_2->addAction(clearScene);
        menu_2->addSeparator();
        menu_2->addAction(actionDelite);
        menu_3->addAction(actionSave);
        menu_3->addAction(actionLoad);
        menu_3->addSeparator();
        menu_3->addAction(actionExport);
        menu_3->addAction(print);

        retranslateUi(MainWindowClass);

        QMetaObject::connectSlotsByName(MainWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowClass)
    {
        MainWindowClass->setWindowTitle(QApplication::translate("MainWindowClass", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionSetItem->setText(QApplication::translate("MainWindowClass", "\320\237\320\260\320\272\320\265\321\202", 0, QApplication::UnicodeUTF8));
        actionSetLine->setText(QApplication::translate("MainWindowClass", "\320\241\320\262\321\217\320\267\321\214", 0, QApplication::UnicodeUTF8));
        actionSetText->setText(QApplication::translate("MainWindowClass", "\320\232\320\276\320\274\320\274\320\265\320\275\321\202\320\260\321\200\320\270\320\271", 0, QApplication::UnicodeUTF8));
        actionDelite->setText(QApplication::translate("MainWindowClass", "\320\243\320\264\320\260\320\273\320\265\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        actionSave->setText(QApplication::translate("MainWindowClass", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        actionLoad->setText(QApplication::translate("MainWindowClass", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214", 0, QApplication::UnicodeUTF8));
        actionExport->setText(QApplication::translate("MainWindowClass", "\320\255\320\272\321\201\320\277\320\276\321\200\321\202 \320\262 \320\272\320\260\321\200\321\202\320\270\320\275\320\272\321\203", 0, QApplication::UnicodeUTF8));
        undo->setText(QApplication::translate("MainWindowClass", "\320\235\320\260\320\267\320\260\320\264", 0, QApplication::UnicodeUTF8));
        rendo->setText(QApplication::translate("MainWindowClass", "\320\222\320\277\320\265\321\200\320\265\320\264", 0, QApplication::UnicodeUTF8));
        print->setText(QApplication::translate("MainWindowClass", "\320\237\320\265\321\207\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        actionExport2->setText(QApplication::translate("MainWindowClass", "\320\255\320\272\321\201\320\277\320\276\321\200\321\202 \321\201\321\206\320\265\320\275\321\213 \320\262 \320\272\320\260\321\200\321\202\320\270\320\275\320\272\321\203", 0, QApplication::UnicodeUTF8));
        action_2->setText(QApplication::translate("MainWindowClass", "\320\234\320\260\321\201\321\201\320\276\320\262\320\276\320\265 \321\201\320\276\320\267\320\264\320\260\320\275\320\270\320\265 \321\215\320\273\320\265\320\274\320\265\320\275\321\202\320\276\320\262 2", 0, QApplication::UnicodeUTF8));
        action_3->setText(QApplication::translate("MainWindowClass", "\320\234\320\260\321\201\321\201\320\276\320\262\320\276\320\265 \321\201\320\276\320\267\320\264\320\260\320\275\320\270\320\265 \321\215\320\273\320\265\320\274\320\265\320\275\321\202\320\276\320\262 2", 0, QApplication::UnicodeUTF8));
        action1->setText(QApplication::translate("MainWindowClass", "1", 0, QApplication::UnicodeUTF8));
        action2->setText(QApplication::translate("MainWindowClass", "2", 0, QApplication::UnicodeUTF8));
        action3->setText(QApplication::translate("MainWindowClass", "3", 0, QApplication::UnicodeUTF8));
        clearScene->setText(QApplication::translate("MainWindowClass", "\320\236\321\207\320\270\321\201\321\202\320\270\321\202\321\214 \320\277\320\276\320\273\320\265", 0, QApplication::UnicodeUTF8));
        btnAddPackege->setText(QApplication::translate("MainWindowClass", "\320\241\320\276\320\267\320\264\320\260\321\202\321\214 \320\277\320\260\320\272\320\265\321\202", 0, QApplication::UnicodeUTF8));
        btnAddconnect->setText(QApplication::translate("MainWindowClass", "\320\241\320\276\320\267\320\264\320\260\321\202\321\214 \321\201\320\262\321\217\320\267\321\214", 0, QApplication::UnicodeUTF8));
        btnAddComment->setText(QApplication::translate("MainWindowClass", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214 \320\272\320\276\320\274\320\265\320\275\321\202\320\260\321\200\320\270\320\271", 0, QApplication::UnicodeUTF8));
        btnDelite->setText(QApplication::translate("MainWindowClass", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        menu->setTitle(QApplication::translate("MainWindowClass", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        menu_4->setTitle(QApplication::translate("MainWindowClass", "\320\234\320\260\321\201\321\201\320\276\320\262\320\276\320\265 \321\201\320\276\320\267\320\264\320\260\320\275\320\270\320\265 \321\215\320\273\320\265\320\274\320\265\320\275\321\202\320\276\320\262", 0, QApplication::UnicodeUTF8));
        menu_2->setTitle(QApplication::translate("MainWindowClass", "\320\240\320\265\320\264\320\260\320\272\321\202\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        menu_3->setTitle(QApplication::translate("MainWindowClass", "\320\244\320\260\320\271\320\273", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindowClass: public Ui_MainWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
