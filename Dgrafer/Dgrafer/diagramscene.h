#ifndef DIAGRAMSCENE_H
#define DIAGRAMSCENE_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include "diagramitem.h"
#include "diagramtextitem.h"

QT_BEGIN_NAMESPACE
class QGraphicsSceneMouseEvent;
class QMenu;
class QPointF;
class QGraphicsLineItem;
class QFont;
class QGraphicsTextItem;
class QColor;
QT_END_NAMESPACE

class DiagramScene : public QGraphicsScene
{
	Q_OBJECT

public:

	enum Mode { InsertItem, InsertLine, InsertText, MoveItem };

	DiagramScene(QMenu *itemMenu, QObject *parent = 0);
	QFont font() const
	{ return myFont; }
	QColor textColor() const
	{ return myTextColor; }
	QColor itemColor() const
	{ return myItemColor; }
	QColor lineColor() const
	{ return myLineColor; }
	void setLineColor(const QColor &color);
	void setTextColor(const QColor &color);
	void setItemColor(const QColor &color);
	void setFont(const QFont &font);
	void insertPacket(QGraphicsSceneMouseEvent *mouseEvent, bool isInsertInPacket);
	void insertPacket(QGraphicsSceneMouseEvent *mouseEvent);
	void insertPacket(int x, int y, int idPacket, int isPlus);
	void insertPacket(int x, int y, bool isInsertInPacket,  int idPacket, int idParentPacket, int isPlus);
	void insertClass(QGraphicsSceneMouseEvent *mouseEvent);
	void insertClass(int x, int y);
	void insertText(int x, int y, QString ref, int w, int s, int c);
	void maxSizePoligon(DiagramItem *item, int countChildrenItems);	//���������� ������
	void minSizePoligon(DiagramItem *item, int countChildrenItems);	//���������� ������
	void banCrossing(QGraphicsItem *item);	// ����������� ������� ���� �� �����
	void delArrow(Arrow * ar);

	

	int type_;
	
	void editCountPacket(int count) //������� ������������ ���������� ������� �� �����
	{
		if(count == 1)
			countPacket++;
		else
			countPacket--;
	}
	int getCountPacket()
		{return countPacket;}
		
	void removePacket(QGraphicsItem * item);	//�������� ������

	public slots:
		void setMode(Mode mode);
		void setItemType(DiagramItem::DiagramType type);
		void editorLostFocusName(DiagramTextItem *item);	//������������ �������� ������
		void editorLostFocusDescription(DiagramTextItem *item);	//��������� �������� ������
		void editorLostClass(DiagramTextItem *item);	//������������ �������� ������
		
		
		Mode returnMode()
			{return myMode;}

signals:
		void itemInserted(DiagramItem *item);
		void textInserted(QGraphicsTextItem *item);
		void itemSelected(QGraphicsItem *item);
		

protected:
	void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
	void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);

private:
	bool isItemChange(int type);
	int maxId;	//����� �������������� ��� ���������� ������
	int zValuePacket;	//���������� z-���������� ���������� ������
	int countPacket;	//���������� ������� �� �����
	void checkingOutScreen(DiagramItem *item);	//�������� ������ �������� �� ������� ������
	


	DiagramItem::DiagramType myItemType;
	QMenu *myItemMenu;
	Mode myMode;
	bool leftButtonDown;
	QPointF startPoint;
	QGraphicsLineItem *line;
	QFont myFont;
	DiagramTextItem *textItem;
	DiagramTextItem *textNameItem;	//����� �������� ������
	DiagramTextItem *textInItem;	//����� � ������
	DiagramTextItem *textPlus;	//������ ��� ���������� �������
	QColor myTextColor;
	QColor myItemColor;
	QColor myLineColor;
};

#endif
