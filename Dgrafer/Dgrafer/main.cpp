#include "mainwindow.h"
#include <QtGui/QApplication>
#include <locale.h> 
#include <QTextCodec>

int main(int argc, char *argv[])
{
	QTextCodec * codec = QTextCodec :: codecForName( "CP1251" ); 
	QTextCodec :: setCodecForTr ( codec ); 
	QTextCodec :: setCodecForLocale ( codec );
	QTextCodec::setCodecForCStrings(codec); 

	QCoreApplication::setOrganizationDomain("OrgDomain");
    QCoreApplication::setOrganizationName("OrgName");
    QCoreApplication::setApplicationName("AppName");
    QCoreApplication::setApplicationVersion("1.0.0");

	QApplication a(argc, argv);
	MainWindow w;
	setlocale(LC_ALL,"rus");
	w.show();
	return a.exec();
}
