#ifndef ARROW_H
#define ARROW_H

#include <QGraphicsLineItem>

#include "diagramitem.h"

QT_BEGIN_NAMESPACE
class QGraphicsPolygonItem;
class QGraphicsLineItem;
class QGraphicsScene;
class QRectF;
class QGraphicsSceneMouseEvent;
class QPainterPath;
QT_END_NAMESPACE

class Arrow : public QGraphicsLineItem
{
public:
	enum { Type = UserType + 4 };

	Arrow(DiagramItem *startItem, DiagramItem *endItem,
		QGraphicsItem *parent = 0, QGraphicsScene *scene = 0);

	int type() const
	{ return Type; }
	QRectF boundingRect() const;
	QPainterPath shape() const;
	void setColor(const QColor &color)
	{ myColor = color; }
	DiagramItem *startItem() const
	{ return myStartItem; }
	DiagramItem *endItem() const
	{ return myEndItem; }
	

	void paint(int x1, int y1, int x2, int y2);

	void updatePosition();
	
	DiagramItem *myStartItem;
	DiagramItem *myEndItem;

protected:
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
		QWidget *widget = 0);
	

private:
	
	QColor myColor;
	QPolygonF arrowHead;
	QPolygonF arrowHead2;
	QPointF temp;
};


#endif
