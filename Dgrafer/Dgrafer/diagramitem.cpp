#include <QtGui>

#include "diagramitem.h"
#include "arrow.h"

DiagramItem::DiagramItem(DiagramType diagramType, QMenu *contextMenu,
	QGraphicsItem *parent, QGraphicsScene *scene)
	: QGraphicsPolygonItem(parent, scene)
{
	myDiagramType = diagramType;
	myContextMenu = contextMenu;

	QPainterPath path;
	switch (myDiagramType) {


	case Step:
			myPolygon	<< QPointF(30, -50) << QPointF(80, -50)
			<< QPointF(80, 50) << QPointF(-70, 50)
			<< QPointF(-70, -80) << QPointF(35, -80)
			<< QPointF(35, -50);

		break;

	case Comment:
            myPolygon	<< QPointF(-60, -40) << QPointF(40, -40) << QPointF(60, -20)
			<< QPointF(40, -20) << QPointF(40, -40) << QPointF(60, -20)
			<< QPointF(60, 40) << QPointF(-60, 40);
		
		break;

	}
	setPolygon(myPolygon);
	setFlag(QGraphicsItem::ItemIsMovable, true);
	setFlag(QGraphicsItem::ItemIsSelectable, true);
	setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
}

bool DiagramItem:: canGetIsParent()
{	
	int type_ = this->type();	//определяем тип элемента
	if(type_ == 65551)	//объект item__ является типом DiagramItem
		return true;
	else
		return false;
}

void DiagramItem::removeArrow(Arrow *arrow)
{
	int index = arrows.indexOf(arrow);

	if (index != -1)
		arrows.removeAt(index);
}

void DiagramItem::removeArrows()
{
	foreach (Arrow *arrow, arrows) {
		arrow->startItem()->removeArrow(arrow);
		arrow->endItem()->removeArrow(arrow);
		scene()->removeItem(arrow);
		delete arrow;
	}
}

DiagramItem * DiagramItem::getItemId(DiagramItem * item__, int id_)
{
	DiagramItem * item;
	QList <QGraphicsItem *> list = item__->scene()->items();
	for(int i =0; i<list.count(); i++)
	{
		if(list.at(i)->type() == 65551)
		{
			item = dynamic_cast<DiagramItem *>(list.at(i));
			/*if(item->getIsComment()==false)
			{*/
				if(id_ == item->returnId())
					return item;
			//}
		}
	}
	return 0;
}

void DiagramItem::addArrow(Arrow *arrow)
{
	arrows.append(arrow);
}

QPixmap DiagramItem::image() const
{
	QPixmap pixmap(250, 250);
	pixmap.fill(Qt::transparent);
	QPainter painter(&pixmap);
	painter.setPen(QPen(Qt::black, 8));
	painter.translate(125, 125);
	painter.drawPolyline(myPolygon);

	return pixmap;
}

void DiagramItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
	//scene()->clearSelection();
	//setSelected(true);
	//myContextMenu->exec(event->screenPos());
}

QVariant DiagramItem::itemChange(GraphicsItemChange change,
	const QVariant &value)
{
	if (change == QGraphicsItem::ItemPositionChange) {
		foreach (Arrow *arrow, arrows) {
			arrow->updatePosition();
		}
	}

	return value;
}

//изменение размеров полигона при увеличении
void DiagramItem::resizePoligonMax()
{
	myPolygon[1].setX(500);

	myPolygon[2].setX(500);
	myPolygon[2].setY(210);

	myPolygon[3].setX(-70);
	myPolygon[3].setY(210);
}

//изменение размеров полигона при уменьшении
void DiagramItem::resizePoligonMin()
{
	myPolygon[1].setX(80);

	myPolygon[2].setX(80);
	myPolygon[2].setY(50);

	myPolygon[3].setX(-70);
	myPolygon[3].setY(50);
}
